<?php /* Template Name: Vendor Profile Page */
get_header();
vendors_only('redirect');
?>

    <section class="container-fluid vendor-area">
        <div class="row">
            <div class="col-lg-3 vendor-nav-wrapper">
                <ul class="vendor-nav">
                    <li><a href="<?php echo get_field('vendor_office_page', 'options'); ?>">forms</a></li>
                    <li><a href="<?php echo get_field('vendor_profile_page', 'options'); ?>" class="active">profile</a>
                    </li>
                    <li><a href="<?php echo wp_logout_url(get_home_url()); ?>">log out</a></li>
                </ul>
            </div>
        </div>
    </section>
    <div class="container vendor-profile-wrapper">
        <?php
        $vendor_user_id = get_current_user_id();
        $vendor_data_id = '';

        $vendors = get_posts(array(
            'numberposts' => -1,
            'post_type' => 'vendors',
            'meta_query' => array(
                array(
                    'key' => 'vendor_login_user',
                    'value' => $vendor_user_id,
                    'compare' => '=',
                )
            )
        ));

        foreach ($vendors as $vendor){
            $vendor_data_id = $vendor->ID;
        }
        ?>
        <div class="row">
            <div class="col-lg-6 col-sm-12 alignright"><p>First Name</p></div><div class="col-lg-6 col-sm-12"><p><?php the_field('vendor_first_name', $vendor_data_id); ?></p></div>
        </div>
        <div class="row">
            <div class="col-lg-6 alignright"><p>Last name</p></div><div class="col-lg-6"><p><?php the_field('vendor_last_name', $vendor_data_id); ?></p></div>
        </div>
        <div class="row">
            <div class="col-lg-6 alignright"><p>Email</p></div><div class="col-lg-6"><p><?php the_field('vendor_company_email', $vendor_data_id); ?></p></div>
        </div>
        <div class="row">
            <div class="col-lg-6 alignright"><p>Mobile Phone</p></div><div class="col-lg-6"><p><?php the_field('vendor_cell_phone', $vendor_data_id); ?></p></div>
        </div>
        <div class="row">
            <div class="col-lg-6 alignright"><p>Company Name</p></div><div class="col-lg-6"><p><?php the_field('vendor_company_name', $vendor_data_id); ?></p></div>
        </div>
        <div class="row">
            <div class="col-lg-6 alignright"><p>Company Website</p></div><div class="col-lg-6"><p><?php the_field('vendor_company_website', $vendor_data_id); ?></p></div>
        </div>
        <div class="row">
            <div class="col-lg-6 alignright"><p>Company Phone</p></div><div class="col-lg-6"><p><?php the_field('vendor_company_phone', $vendor_data_id); ?></p></div>
        </div>
        <div class="row">
            <div class="col-lg-6 alignright"><p>City</p></div><div class="col-lg-6"><p><?php the_field('vendor_city', $vendor_data_id); ?></p></div>
        </div>
        <div class="row">
            <div class="col-lg-6 alignright"><p>State</p></div><div class="col-lg-6"><p><?php the_field('vendor_state', $vendor_data_id); ?></p></div>
        </div>
        <div class="row">
            <div class="col-lg-6 alignright"><p>Referral Link</p></div><div class="col-lg-6"><p><a href="<?php echo get_field('vendor_application_page','options')."?vendor_id=".$vendor_data_id; ?>"><?php echo get_field('vendor_application_page','options')."?vendor_id=".$vendor_data_id; ?></a></p></div>
        </div>
        <div class="row">
            <div class="col-lg-6 alignright"><p>Change Password</p></div><div class="col-lg-6"><form class="change-pass"><input type="password" required class="newpass"><a href="#" class="showpass eye"></a><input type="submit"><div class="form-message"></div></form></div>
        </div>
    </div>
<?php
get_footer();
?>