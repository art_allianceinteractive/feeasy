jQuery(document).ready(function ($) {
    /* smooth scroll to */

    if (document.location.pathname == '/') {
        document.querySelectorAll('a[href^="/#"]').forEach(anchor => {
            if (!anchor.classList.contains('nav-open') && !anchor.classList.contains('nav-close')) {
                anchor.addEventListener('click', function (e) {
                    e.preventDefault();
                    element = document.querySelector(this.getAttribute('href').replace("/", ""));
                    offset = 85;
                    bodyRect = document.body.getBoundingClientRect().top;
                    elementRect = element.getBoundingClientRect().top;
                    elementPosition = elementRect - bodyRect;
                    offsetPosition = elementPosition - offset;

                    scrollTo({
                        top: offsetPosition,
                        behavior: 'smooth'
                    });
                });
            }
        });
    }

    /* mobile nav */

    function nav_close() {

        if (window.screen.width >= 768 && window.screen.width < 992){
            menuWidth = '40%';
        } else {
            menuWidth = '85%';
        }

        if (document.querySelector('.basic-header .nav-wrapper-mobile')) {
            document.querySelector('.basic-header .nav-wrapper-mobile').style.width = '0px';
            document.querySelector('.basic-header .nav-wrapper-mobile').style.padding = '0px';
            document.querySelector('.basic-header .nav-wrapper-mobile').style.right = '-'+menuWidth;
            document.querySelector('.basic-header .nav-wrapper-mobile').style.opacity = '0';
        }

        if (document.querySelector('.full-screen-header .nav-wrapper-mobile')) {
            document.querySelector('.full-screen-header .nav-wrapper-mobile').style.width = '0px';
            document.querySelector('.full-screen-header .nav-wrapper-mobile').style.padding = '0px';
            document.querySelector('.full-screen-header .nav-wrapper-mobile').style.right = '-'+menuWidth;
            document.querySelector('.full-screen-header .nav-wrapper-mobile').style.opacity = '0';
        }
    }

    function nav_open() {

        if (window.screen.width >= 768 && window.screen.width < 1200){
            menuWidth = '40%';
        } else {
            menuWidth = '85%';
        }

        if (document.querySelector('.basic-header .nav-wrapper-mobile')) {
            document.querySelector('.basic-header .nav-wrapper-mobile').style.right = 0;
            document.querySelector('.basic-header .nav-wrapper-mobile').style.width = menuWidth;
            document.querySelector('.basic-header .nav-wrapper-mobile').style.padding = '0px 25px 0px 15px';
            document.querySelector('.basic-header .nav-wrapper-mobile').style.opacity = '1';
        }

        if (document.querySelector('.full-screen-header .nav-wrapper-mobile')) {
            document.querySelector('.full-screen-header .nav-wrapper-mobile').style.right = 0;
            document.querySelector('.full-screen-header .nav-wrapper-mobile').style.width = menuWidth;
            document.querySelector('.full-screen-header .nav-wrapper-mobile').style.padding = '0px 25px 0px 15px';
            document.querySelector('.full-screen-header .nav-wrapper-mobile').style.opacity = '1';
        }
    }

    document.querySelector('.nav-open').addEventListener('click', function (e) {
        e.preventDefault();
        nav_open();
    });

    document.querySelector('.nav-close').addEventListener('click', function (e) {
        e.preventDefault();
        nav_close();
    });

    document.querySelector('.nav-wrapper-mobile').addEventListener('click', function (e) {
        if (e.target.classList.contains('nav-link')) {
            nav_close();
        }
    });

    /* sticky */

    document.addEventListener('scroll', function (e) {

        scroll_limit = 30;

        if (window.scrollY > 30) {
            if (document.querySelector('.full-screen-header .row')) {
                document.querySelector('.full-screen-header .row').classList.add('main-nav-sticky');
            }
            if (document.querySelector('.basic-header .row')) {
                document.querySelector('.basic-header .row').classList.add('main-nav-sticky');
            }
        }

        if (window.scrollY <= 30) {
            if (document.querySelector('.full-screen-header .row')) {
                document.querySelector('.full-screen-header .row').classList.remove('main-nav-sticky');
            }
            if (document.querySelector('.basic-header .row')) {
                document.querySelector('.basic-header .row').classList.remove('main-nav-sticky');
            }
        }
    });


    /* Tables */

    if (document.querySelector('#vendor-forms')) {
        $('#vendor-forms').DataTable();
    }


    /* change pass*/
    if (document.querySelector('form.change-pass')) {
        document.querySelector('form.change-pass input[type=submit]').addEventListener('click', function (e) {
            e.preventDefault();
            passval = document.querySelector('form.change-pass .newpass').value;

            var data = {
                action: 'vendor_change_pass',
                security: MyAjax.security,
                passval: passval
            };

            $.post(MyAjax.ajaxurl, data, function (response) {
                document.querySelector('form.change-pass .form-message').innerHTML = response;
                document.querySelector('form.change-pass .form-message').style.opacity = '1';
            });

        })

        // show pass
        document.querySelector('form.change-pass .showpass').addEventListener('click', function(e){
            e.preventDefault();
            button = e.target;
            if(button.classList.contains('eye')){
                button.classList.remove('eye');
                button.classList.add('eye-off');
                document.querySelector('form.change-pass .newpass').type = "text";
            } else {
                button.classList.add('eye');
                button.classList.remove('eye-off');
                document.querySelector('form.change-pass .newpass').type = "password";
            }
        })

    }

});

