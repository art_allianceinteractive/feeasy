<?php
/*-------------------------------------------------*/
/*	Lead generation
/*-------------------------------------------------*/

add_action('gform_after_submission_' . get_field('lead_generation_form', 'options'), 'lead_generation', 10, 2);
function lead_generation($entry, $form)
{
    $form_data = gf_entry_normalize($entry, $form);

    $lead_arr = array(
        'post_title' => "Client: " . $form_data['client-first-name'] . " " . $form_data['client-last-name'] . "; Agent: " . $form_data['agent-first-name'] . " " . $form_data['agent-last-name'],
        'post_status' => 'publish',
        'post_author' => 1,
        'post_type' => 'leads'
    );
    $lead_data = wp_insert_post($lead_arr);

    if (is_wp_error($lead_data)) {
        echo "<!-- ";
        var_dump($lead_data->get_error_message());
        echo " -->";
    } else {
        update_field('client_first_name', $form_data['client-first-name'], $lead_data);
        update_field('client_last_name', $form_data['client-last-name'], $lead_data);
        update_field('client_phone_number', $form_data['client-phone-number'], $lead_data);
        update_field('client_email_address', $form_data['client-email-address'], $lead_data);
        update_field('client_loan_amount', $form_data['client-loan-amount'], $lead_data);
        update_field('agent_first_name', $form_data['agent-first-name'], $lead_data);
        update_field('agent_last_name', $form_data['agent-last-name'], $lead_data);
        update_field('agent_phone_number', $form_data['agent-phone-number'], $lead_data);
        update_field('agent_email_address', $form_data['agent-email-address'], $lead_data);
        update_field('agent_city', $form_data['agent-city'], $lead_data);
        update_field('agent_state', $form_data['agent-state'], $lead_data);
        update_field('agent_company', $form_data['agent-company'], $lead_data);
        update_field('agent_message', $form_data['agent-message'], $lead_data);
    }

    //client notify
    $to = $form_data['client-email-address'];
    $subject = get_field('lead_form_sent_to_client_subject', 'options');
    $body = get_field('lead_form_sent_to_client', 'options');
    $shortcodes = array("[vendor-name]", "[client-first-name]");
    $values   = array($form_data['agent-first-name']." ".$form_data['agent-last-name'], $form_data['client-first-name']);
    $body = str_replace($shortcodes, $values, $body);
    $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Feeasy <info@itsfeeasy.com>');
    wp_mail( $to, $subject, $body, $headers );

    //agent notify
    $to = $form_data['agent-email-address'];
    $subject = get_field('lead_form_sent_to_vendor_subject', 'options');
    $body = get_field('lead_form_sent_to_vendor', 'options');
    $shortcodes = array("[vendor-first-name]");
    $values   = array($form_data['agent-first-name']);
    $body = str_replace($shortcodes, $values, $body);
    $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Feeasy <info@itsfeeasy.com>');
    wp_mail( $to, $subject, $body, $headers );

}

add_action('admin_footer', 'leads_ajax');
function leads_ajax()
{
    $ajax_nonce = wp_create_nonce("s2h3f423f4u23g4");
    ?>
    <script>
        if (document.querySelector("#leads-forms")) {
            document.querySelector("#leads-forms").addEventListener('click', function (e) {
                button = e.target;
                console.log(button.parentNode);
                if (button.parentNode.classList.contains('contacted-btn')) {
                    console.log('contacted');
                    jQuery.ajax({
                        type: "POST",
                        url: ajaxurl,
                        data: {
                            action: 'lead_contacted',
                            security: '<?php echo $ajax_nonce; ?>',
                            lead_id: button.parentNode.id
                        },
                        success: function (response) {
                            resp_arr = JSON.parse(response);

                            if (resp_arr['status'] == 'success') {
                                button.parentNode.classList.remove('contacted-btn');
                                button.parentNode.classList.add('mark_contacted-btn');
                                button.parentNode.innerHTML = '<span class="dashicons dashicons-warning"></span>';
                            } else {
                                alert(resp_arr['message']);
                            }

                            console.log(resp_arr['message']);
                        }
                    });
                } else if (button.parentNode.classList.contains('mark_contacted-btn')) {
                    console.log('disapproval');
                    jQuery.ajax({
                        type: "POST",
                        url: ajaxurl,
                        data: {
                            action: 'lead_need_contact',
                            security: '<?php echo $ajax_nonce; ?>',
                            lead_id: button.parentNode.id
                        },
                        success: function (response) {
                            resp_arr = JSON.parse(response);

                            if (resp_arr['status'] == 'success') {
                                button.parentNode.classList.add('contacted-btn');
                                button.parentNode.classList.remove('mark_contacted-btn');
                                button.parentNode.innerHTML = '<span class="dashicons dashicons-yes-alt"></span>';
                            } else {
                                alert(resp_arr['message']);
                            }

                            console.log(resp_arr['message']);
                        }
                    });
                } else if (button.parentNode.classList.contains('remove-btn')) {
                    confirm_removal = confirm("Confirm removal");
                    if (confirm_removal == true) {
                        jQuery.ajax({
                            type: "POST",
                            url: ajaxurl,
                            data: {
                                action: 'lead_remove',
                                security: '<?php echo $ajax_nonce; ?>',
                                lead_id: button.parentNode.id
                            },
                            success: function (response) {
                                resp_arr = JSON.parse(response);

                                if (resp_arr['status'] == 'success') {
                                    location.reload();
                                } else {
                                    alert(resp_arr['message']);
                                }

                                console.log(resp_arr['message']);
                            }
                        });
                    }
                }
            });
        }
    </script>
    <?php
}

function lead_remove_callback()
{
    check_ajax_referer('s2h3f423f4u23g4', 'security');
    $lead_id = $_POST['lead_id'];

    if (wp_delete_post($_POST['lead_id'], true)) {
        echo json_encode(array(
            'status' => 'success',
            'message' => 'Lead Removed'
        ));
    }

    wp_die();
}

add_action('wp_ajax_lead_remove', 'lead_remove_callback');
add_action('wp_ajax_nopriv_lead_remove', 'lead_remove_callback');


function lead_contacted_callback()
{
    check_ajax_referer('s2h3f423f4u23g4', 'security');
    $lead_id = $_POST['lead_id'];
    if (update_field('client_contacted', 0, $lead_id)) {
        echo json_encode(array(
            'status' => 'success',
            'message' => 'Lead Contacted'
        ));
    } else {
        echo json_encode(array(
            'status' => 'error',
            'message' => 'Unknown Error ' . $_POST['lead_id'],
        ));
    }

    wp_die();
}

add_action('wp_ajax_lead_contacted', 'lead_contacted_callback');
add_action('wp_ajax_nopriv_lead_contacted', 'lead_contacted_callback');


function lead_need_contact_callback()
{
    check_ajax_referer('s2h3f423f4u23g4', 'security');
    $lead_id = $_POST['lead_id'];
    if (update_field('client_contacted', 1, $lead_id)) {
        echo json_encode(array(
            'status' => 'success',
            'message' => 'Lead Contacted'
        ));
    } else {
        echo json_encode(array(
            'status' => 'error',
            'message' => 'Unknown Error ' . $_POST['lead_id'],
        ));
    }

    wp_die();
}

add_action('wp_ajax_lead_need_contact', 'lead_need_contact_callback');
add_action('wp_ajax_nopriv_lead_need_contact', 'lead_need_contact_callback');


/*-------------------------------------------------*/
/*  Register a custom menu pages Leads.
/*-------------------------------------------------*/
function register_custom_menu_page_leads()
{
    add_menu_page(
        __('Leads', 'textdomain'),
        'Leads',
        'manage_options',
        'leads',
        'get_leads',
        'dashicons-businessperson',
        6
    );
}

add_action('admin_menu', 'register_custom_menu_page_leads');

function get_leads()
{ ?>
    <div class="wrap">
        <h1 class="wp-heading-inline">Leads</h1>
        <table id="leads-forms" class="display js-table" style="width:100%">
            <thead>
            <tr>
                <th>Submitted</th>
                <th>Client Name</th>
                <th>Client Phone</th>
                <th>Client Email</th>
                <th>Loan Amount</th>
                <th>Agent Name</th>
                <th>Agent Phone</th>
                <th>Agent Email</th>
                <th>Agent City</th>
                <th>Agent State</th>
                <th>Agent Company</th>
                <th>Message</th>
                <th>Admin Note</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php $leads = get_posts(array(
                'numberposts' => -1,
                'post_type' => 'leads'
            ));

            foreach($leads as $lead){
            ?>
            <tr>
                <td><?php echo get_the_date('Y-m-d H:i:s', $lead->ID); ?></td>
                <td><?php the_field('client_first_name', $lead->ID); ?> <?php the_field('client_last_name', $lead->ID); ?></td>
                <td><?php the_field('client_phone_number', $lead->ID); ?></td>
                <td><?php the_field('client_email_address', $lead->ID); ?></td>
                <td><?php the_field('client_loan_amount', $lead->ID); ?></td>
                <td><?php the_field('agent_first_name', $lead->ID); ?> <?php the_field('agent_last_name', $lead->ID); ?></td>
                <td><?php the_field('agent_phone_number', $lead->ID); ?></td>
                <td><?php the_field('agent_email_address', $lead->ID); ?></td>
                <td><?php the_field('agent_city', $lead->ID); ?></td>
                <td><?php the_field('agent_state', $lead->ID); ?></td>
                <td><?php the_field('agent_company', $lead->ID); ?></td>
                <td><?php the_field('agent_message', $lead->ID); ?></td>
                <td><?php the_field('admin_note', $lead->ID); ?></td>
                <td>
                    <div class="btn-wrapper"><?php if (get_field('client_contacted', $lead->ID)) { ?>
                            <a href="#" title="Contacted" id="<?php echo $lead->ID; ?>"
                               class="button action contacted-btn"><span class="dashicons dashicons-yes-alt"></span></a>
                        <?php } else { ?>
                            <a href="#" title="Mark As Contacted" id="<?php echo $lead->ID; ?>"
                               class="button action mark_contacted-btn"><span
                                        class="dashicons dashicons-warning"></span></a>
                        <?php } ?>
                        <a href="<?php echo get_edit_post_link($lead->ID); ?>" title="Edit Lead" target="_blank"
                           class="button action edit-btn"><span class="dashicons dashicons-edit"></span></a>
                        <a href="#" title="Remove Lead" id="<?php echo $lead->ID; ?>"
                           class="button action remove-btn"><span class="dashicons dashicons-trash"></span></a>
                    </div>
                </td>
            </tr>
            <?php } ?>
            </tbody>
            <tfoot>
            <tr>
                <th>Submitted</th>
                <th>Client Name</th>
                <th>Client Phone</th>
                <th>Client Email</th>
                <th>Loan Amount</th>
                <th>Agent Name</th>
                <th>Agent Phone</th>
                <th>Agent Email</th>
                <th>Agent City</th>
                <th>Agent State</th>
                <th>Agent Company</th>
                <th>Message</th>
                <th>Admin Note</th>
                <th>Actions</th>
            </tr>
            </tfoot>
        </table>
        <div class="csv-gen-btns">
            <a class="csv-gen" target="_blank" href="<?php the_field('leads_export_page', 'options'); ?>">Get Leads CSV</a>
        </div>
    </div>
<?php } ?>
