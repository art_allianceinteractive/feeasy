<?php
/*-------------------------------------------------*/
/*	Vendor registration and approval
/*-------------------------------------------------*/

add_action('login_form_register', function () {
    wp_redirect(get_field('vendor_registration_page', 'options'));
    exit;
});

add_action('gform_after_submission_' . get_field('vendor_registration_form', 'options'), 'vendor_registration', 10, 2);
function vendor_registration($entry, $form)
{
    $form_data = gf_entry_normalize($entry, $form);

    $vendor_arr = array(
        'post_title' => $form_data['first-name'] . " " . $form_data['last-name'],
        'post_status' => 'publish',
        'post_author' => 1,
        'post_type' => 'vendors'
    );
    $vendor_data = wp_insert_post($vendor_arr);

    if (is_wp_error($vendor_data)) {
        echo "<!-- ";
        var_dump($vendor_data->get_error_message());
        echo " -->";
    } else {
        update_field('vendor_company_email', $form_data['email'], $vendor_data);
        update_field('vendor_company_name', $form_data['company-name'], $vendor_data);
        update_field('vendor_company_website', $form_data['company-website'], $vendor_data);
        update_field('vendor_company_phone', $form_data['company-phone-number'], $vendor_data);
        update_field('vendor_first_name', $form_data['first-name'], $vendor_data);
        update_field('vendor_last_name', $form_data['last-name'], $vendor_data);
        update_field('vendor_cell_phone', $form_data['cell-phone'], $vendor_data);
        update_field('vendor_city', $form_data['city'], $vendor_data);
        update_field('vendor_state', $form_data['state'], $vendor_data);
        update_field('vendor_status', 'pending', $vendor_data);
    }

    //admin notify
    $to = get_option( 'admin_email' );
    $subject = get_field('vendor_registration_to_admin_subject', 'options');
    $body = get_field('vendor_registration_to_admin', 'options');
    $shortcodes = array("[vendor-first-name]", "[vendor-last-name]", "[vendor-email]", "[vendor-phone]", "[vendor-company-name]", "[vendor-company-website]", "[vendor-company-phone]", "[vendor-city]", "[vendor-state]");
    $values   = array($form_data['first-name'], $form_data['last-name'], $form_data['email'], $form_data['cell-phone'], $form_data['company-name'], $form_data['company-website'], $form_data['company-phone-number'], $form_data['city'], $form_data['state']);
    $body = str_replace($shortcodes, $values, $body);
    $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Feeasy <info@itsfeeasy.com>');
    wp_mail( $to, $subject, $body, $headers );

    //vendor notify
    /*
    $to = $form_data['email'];
    $subject = get_field('vendor_registration_to_vendor_subject', 'options');
    $body = get_field('vendor_registration_to_vendor', 'options');
    $shortcodes = array("[vendor-first-name]");
    $values   = array($form_data['first-name']);
    $body = str_replace($shortcodes, $values, $body);
    $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Feeasy <info@itsfeeasy.com>');
    wp_mail( $to, $subject, $body, $headers );
    */
}

add_shortcode( 'vendor-first-name', 'get_vendor_first_name' );
function get_vendor_first_name() {
    return $_GET['vname']? $_GET['vname'] : 'vendor';
}

add_action('admin_footer', 'vendor_approval_ajax');
function vendor_approval_ajax()
{
    $ajax_nonce = wp_create_nonce("s2h3f423f4u23g4");
    ?>
    <script>
        if (document.querySelector("#vendor-forms")) {
            document.querySelector("#vendor-forms").addEventListener('click', function (e) {
                button = e.target;
                console.log(button.parentNode);
                if (button.parentNode.classList.contains('approval-btn')) {
                    console.log('approval');
                    jQuery.ajax({
                        type: "POST",
                        url: ajaxurl,
                        data: {
                            action: 'vendor_approval',
                            security: '<?php echo $ajax_nonce; ?>',
                            vendor_id: button.parentNode.id
                        },
                        success: function (response) {
                            resp_arr = JSON.parse(response);

                            if (resp_arr['status'] == 'success') {
                                button.parentNode.classList.remove('approval-btn');
                                button.parentNode.classList.add('disapproval-btn');
                                button.parentNode.innerHTML = '<span class="dashicons dashicons-dismiss"></span>';
                            } else {
                                alert(resp_arr['message']);
                            }

                            console.log(resp_arr['message']);
                        }
                    });
                } else if (button.parentNode.classList.contains('disapproval-btn')) {
                    console.log('disapproval');
                    jQuery.ajax({
                        type: "POST",
                        url: ajaxurl,
                        data: {
                            action: 'vendor_disapproval',
                            security: '<?php echo $ajax_nonce; ?>',
                            vendor_id: button.parentNode.id
                        },
                        success: function (response) {
                            resp_arr = JSON.parse(response);

                            if (resp_arr['status'] == 'success') {
                                button.parentNode.classList.add('approval-btn');
                                button.parentNode.classList.remove('disapproval-btn');
                                button.parentNode.innerHTML = '<span class="dashicons dashicons-yes-alt"></span>';
                            } else {
                                alert(resp_arr['message']);
                            }

                            console.log(resp_arr['message']);
                        }
                    });
                } else if (button.parentNode.classList.contains('remove-btn')) {
                    confirm_removal = confirm("Confirm removal");
                    if (confirm_removal == true) {
                        jQuery.ajax({
                            type: "POST",
                            url: ajaxurl,
                            data: {
                                action: 'vendor_remove',
                                security: '<?php echo $ajax_nonce; ?>',
                                vendor_id: button.parentNode.id
                            },
                            success: function (response) {
                                resp_arr = JSON.parse(response);

                                if (resp_arr['status'] == 'success') {
                                    location.reload();
                                } else {
                                    alert(resp_arr['message']);
                                }

                                console.log(resp_arr['message']);
                            }
                        });
                    }
                }
            });
        }
    </script>
    <?php
}

function vendor_remove_callback()
{
    check_ajax_referer('s2h3f423f4u23g4', 'security');
    $vendor_id = $_POST['vendor_id'];
    $v_user = get_field('vendor_login_user', $vendor_id);

    $vendor_rem = false;
    $user_rem = false;

    if (wp_delete_user($v_user)) {

        //vendor notify
        $to = get_field('vendor_company_email', $vendor_id);
        $subject = get_field('vendor_disapproved_to_vendor_subject', 'options');
        $body = get_field('vendor_disapproved_to_vendor', 'options');
        $shortcodes = array("[vendor-first-name]");
        $values = array(get_field('vendor_first_name', $vendor_id));
        $body = str_replace($shortcodes, $values, $body);
        $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Feeasy <info@itsfeeasy.com>');
        wp_mail($to, $subject, $body, $headers);

        $user_rem = true;
    }
    if (wp_delete_post($_POST['vendor_id'], true)) {
        $vendor_rem = true;
    }

    if ($vendor_rem == true) {
        echo json_encode(array(
            'status' => 'success',
            'message' => 'vendor Removed'
        ));
    } else {
        echo json_encode(array(
            'status' => 'error',
            'message' => 'Error. Check if vendor exists'
        ));
    }

    wp_die();
}

add_action('wp_ajax_vendor_remove', 'vendor_remove_callback');
add_action('wp_ajax_nopriv_vendor_remove', 'vendor_remove_callback');


function vendor_approval_callback()
{
    check_ajax_referer('s2h3f423f4u23g4', 'security');
    $vendor_id = $_POST['vendor_id'];
    $pass = wp_generate_password();
    $userdata = array(
        'user_pass' => $pass,   //(string) The plain-text user password.
        'user_login' => get_field('vendor_company_email', $vendor_id),   //(string) The user's login username.
        'user_nicename' => sanitize_title(get_field('vendor_first_name', $vendor_id) . " " . get_field('vendor_last_name', $vendor_id)),   //(string) The URL-friendly user name.
        'user_email' => get_field('vendor_company_email', $vendor_id),   //(string) The user email address.
        'display_name' => get_field('vendor_first_name', $vendor_id) . " " . get_field('vendor_last_name', $vendor_id),   //(string) The user's display name. Default is the user's username.
        'nickname' => sanitize_title(get_field('vendor_first_name', $vendor_id) . " " . get_field('vendor_last_name', $vendor_id)),   //(string) The user's nickname. Default is the user's username.
        'show_admin_bar_front' => false,   //(string|bool) Whether to display the Admin Bar for the user on the site's front end. Default true.
        'role' => 'vendor',   //(string) User's role.
    );

    $new_user = wp_insert_user($userdata);

    if (is_wp_error($new_user)) {
        echo json_encode(array(
            'status' => 'error',
            'message' => $new_user->get_error_message()
        ));
    } else {
        update_field('vendor_status', 'approved', $vendor_id);
        update_field('vendor_login_user', $new_user, $vendor_id);

        //vendor notify
        $to = get_field('vendor_company_email', $vendor_id);
        $subject = get_field('vendor_approved_to_vendor_subject', 'options');
        $body = get_field('vendor_approved_to_vendor', 'options');
        $shortcodes = array("[vendor-first-name]", "[vendor-login]", "[vendor-pass]", "[vendor-login-link]");
        $values   = array(get_field('vendor_first_name', $vendor_id), get_field('vendor_company_email', $vendor_id), $pass, '<a href="'.get_field('vendor_login_page', 'options').'">Login</a>');
        $body = str_replace($shortcodes, $values, $body);
        $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Feeasy <info@itsfeeasy.com>');
        wp_mail( $to, $subject, $body, $headers );

        echo json_encode(array(
            'status' => 'success',
            'message' => 'User Created',
            'user_id' => $new_user
        ));
    }
    wp_die();
}

add_action('wp_ajax_vendor_approval', 'vendor_approval_callback');
add_action('wp_ajax_nopriv_vendor_approval', 'vendor_approval_callback');

function vendor_disapproval_callback()
{
    check_ajax_referer('s2h3f423f4u23g4', 'security');
    $vendor_id = $_POST['vendor_id'];
    $v_user = get_field('vendor_login_user', $vendor_id);

    if (wp_delete_user($v_user)) {

        //vendor notify
        $to = get_field('vendor_company_email', $vendor_id);
        $subject = get_field('vendor_disapproved_to_vendor_subject', 'options');
        $body = get_field('vendor_disapproved_to_vendor', 'options');
        $shortcodes = array("[vendor-first-name]");
        $values   = array(get_field('vendor_first_name', $vendor_id));
        $body = str_replace($shortcodes, $values, $body);
        $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Feeasy <info@itsfeeasy.com>');
        wp_mail( $to, $subject, $body, $headers );

        echo json_encode(array(
            'status' => 'success',
            'message' => 'User Removed'
        ));
    } else {
        echo json_encode(array(
            'status' => 'error',
            'message' => 'Error. Check if user exists'
        ));
    }

    wp_die();
}

add_action('wp_ajax_vendor_disapproval', 'vendor_disapproval_callback');
add_action('wp_ajax_nopriv_vendor_disapproval', 'vendor_disapproval_callback');


/*-------------------------------------------------*/
/*  Register a custom menu pages Vendors.
/*-------------------------------------------------*/
function register_custom_menu_page_vendor()
{
    add_menu_page(
        __('Vendors', 'textdomain'),
        'Vendors',
        'manage_options',
        'vendors',
        'get_vendors',
        'dashicons-businessperson',
        6
    );
}

add_action('admin_menu', 'register_custom_menu_page_vendor');



/*-------------------------------------------------*/
/*  Ajax Change Pass.
/*-------------------------------------------------*/
add_action( 'wp_ajax_vendor_change_pass', 'vendor_change_pass_callback' );
add_action( 'wp_ajax_nopriv_vendor_change_pass', 'vendor_change_pass_callback' );
function vendor_change_pass_callback() {
    check_ajax_referer( 'ml456knjdjf34n3fn309m93', 'security' );

    $password = $_POST['passval'];
    $uppercase = preg_match('@[A-Z]@', $password);
    $lowercase = preg_match('@[a-z]@', $password);
    $number    = preg_match('@[0-9]@', $password);
    $specialChars = preg_match('@[^\w]@', $password);

    if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) {
        echo "<p style='color:#6d221f'>Password should be at least 8 characters in length and should include at least one upper case letter, one number, and one special character.</p>";
    } else {
        wp_set_password(wp_slash($password), get_current_user_id());
        echo "<p style='color:#1f6d1f'>The password was updated successfully. Please <a href='".get_field('vendor_login_page', 'options')."'>log in</a> using your new password.</p>";
    }

    die();
}



function get_vendors()
{ ?>
    <div class="wrap">
        <h1 class="wp-heading-inline">Vendors</h1>
        <table id="vendor-forms" class="display js-table" style="width:100%">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile Phone</th>
                <th>Company Name</th>
                <th>Company Website</th>
                <th>Company Phone</th>
                <th>City</th>
                <th>State</th>
                <th>Refferal URL</th>
                <th>Created Date</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php $vendors = get_posts(array(
                'numberposts' => -1,
                'post_type' => 'vendors'
            ));

            foreach($vendors as $vendor){
            ?>
            <tr>
                <td><?php the_field('vendor_first_name', $vendor->ID); ?> <?php the_field('vendor_last_name', $vendor->ID); ?></td>
                <td><?php the_field('vendor_company_email', $vendor->ID); ?></td>
                <td><?php the_field('vendor_cell_phone', $vendor->ID); ?></td>
                <td><?php the_field('vendor_company_name', $vendor->ID); ?></td>
                <td><?php the_field('vendor_company_website', $vendor->ID); ?></td>
                <td><?php the_field('vendor_company_phone', $vendor->ID); ?></td>
                <td><?php the_field('vendor_city', $vendor->ID); ?></td>
                <td><?php the_field('vendor_state', $vendor->ID); ?></td>
                <td><a href="<?php echo get_field('vendor_application_page','options')."?vendor_id=".$vendor->ID; ?>" target="_blank">Refferal URL</a></td>
                <td><?php echo get_the_date('Y-m-d', $vendor->ID); ?></td>
                <td><div class="btn-wrapper">
                    <?php if (get_field('vendor_login_user', $vendor->ID)) { ?>
                        <a href="#" title="Disapprove Vendor" id="<?php echo $vendor->ID; ?>"
                           class="button action disapproval-btn"><span
                                    class="dashicons dashicons-dismiss"></span></a>
                    <?php } else { ?>
                        <a href="#" title="Approve Vendor" id="<?php echo $vendor->ID; ?>"
                           class="button action approval-btn"><span class="dashicons dashicons-yes-alt"></span></a>
                    <?php } ?>
                    <a href="<?php echo get_edit_post_link($vendor->ID); ?>" title="Edit Vendor" target="_blank"
                       class="button action edit-btn"><span class="dashicons dashicons-edit"></span></a>
                    <a href="#" title="Remove Vendor" id="<?php echo $vendor->ID; ?>"
                       class="button action remove-btn"><span class="dashicons dashicons-trash"></span></a>
                </div>
                </td>
            </tr>
            <?php } ?>
            </tbody>
            <tfoot>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile Phone</th>
                <th>Company Name</th>
                <th>Company Website</th>
                <th>Company Phone</th>
                <th>City</th>
                <th>State</th>
                <th>Refferal URL</th>
                <th>Created Date</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
        <div class="csv-gen-btns">
            <a class="csv-gen" target="_blank" href="<?php the_field('vendors_export_page', 'options'); ?>">Get Vendors CSV</a>
        </div>
    </div>
<?php } ?>
