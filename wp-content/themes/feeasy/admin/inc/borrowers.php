<?php
/*-------------------------------------------------*/
/*  Register a custom menu pages Borrowers.
/*-------------------------------------------------*/
function register_custom_menu_page_borrower()
{
    add_menu_page(
        __('Borrowers', 'textdomain'),
        'Borrowers',
        'manage_options',
        'borrowers',
        'get_borrowers',
        'dashicons-admin-users',
        6
    );
}

add_action('admin_menu', 'register_custom_menu_page_borrower');

add_shortcode( 'borrowers-first-name', 'get_borrower_first_name' );
function get_borrower_first_name() {
    return $_GET['bname']? $_GET['bname'] : 'borrower';
}

add_action('gform_after_submission_' . get_field('application_form', 'options'), 'borrower_registration', 10, 2);
function borrower_registration($entry, $form)
{
    $form_data = gf_entry_normalize($entry, $form);

    $borrower_arr = array(
        'post_title' => $form_data['first-name'] . " " . $form_data['last-name'],
        'post_status' => 'publish',
        'post_author' => 1,
        'post_type' => 'borrowers'
    );
    $borrower_data = wp_insert_post($borrower_arr);

    if (is_wp_error($borrower_data)) {
        echo "<!-- ";
        var_dump($borrower_data->get_error_message());
        echo " -->";
    } else {
        update_field('borrower_loan_amount', $form_data['loan-amount'], $borrower_data);
        update_field('borrower_loan_purpose', $form_data['loan-purpose'], $borrower_data);
        update_field('borrower_estimated_credit_score', $form_data['estimated-credit-score'], $borrower_data);
        update_field('borrower_first_name', $form_data['first-name'], $borrower_data);
        update_field('borrower_last_name', $form_data['last-name'], $borrower_data);
        update_field('borrower_date_of_birth', $form_data['date-of-birth'], $borrower_data);
        update_field('borrower_email', $form_data['email'], $borrower_data);
        update_field('borrower_primary_phone', $form_data['primary-phone'], $borrower_data);
        update_field('borrower_highest_education_level', $form_data['highest-education-level'], $borrower_data);
        update_field('borrower_co_borrower', $form_data['would-you-like-to-add-a-co-borrower'], $borrower_data);
        update_field('co_borrower_first_name', $form_data['co-borrower-first-name'], $borrower_data);
        update_field('co_borrower_last_name', $form_data['co-borrower-last-name'], $borrower_data);
        update_field('co_borrower_date_of_birth', $form_data['co-borrower-date-of-birth'], $borrower_data);
        update_field('co_borrower_address', $form_data['co-borrower-address']['street-address'], $borrower_data);
        update_field('co_borrower_city', $form_data['co-borrower-address']['city'], $borrower_data);
        update_field('co_borrower_state', $form_data['co-borrower-address']['state-province'], $borrower_data);
        update_field('co_borrower_zip_code', $form_data['co-borrower-address']['zip-postal-code'], $borrower_data);
        update_field('co_borrower_annual_pre_tax_income', $form_data['co-borrower-annual-pre-tax-income'], $borrower_data);
        update_field('borrower_home_address', $form_data['borrower-address']['street-address'], $borrower_data);
        update_field('borrower_city', $form_data['borrower-address']['city'], $borrower_data);
        update_field('borrower_state', $form_data['borrower-address']['state-province'], $borrower_data);
        update_field('borrower_zip_code', $form_data['borrower-address']['zip-postal-code'], $borrower_data);
        update_field('borrower_residence_type', $form_data['residence-type'], $borrower_data);
        update_field('borrower_monthly_payment', $form_data['monthly-payment'], $borrower_data);
        update_field('borrower_time_at_address_years', $form_data['time-at-address'], $borrower_data);
        update_field('borrower_time_at_address_month', $form_data['months-at-address']? $form_data['months-at-address'] : 0, $borrower_data);
        update_field('borrower_employment_status', $form_data['employment-status'], $borrower_data);
        update_field('borrower_monthly_income', $form_data['monthly-income'], $borrower_data);
        update_field('borrower_social_security_number', preg_replace('/[^0-9]/', '', $form_data['social-security-number']), $borrower_data);
        update_field('borrower_employer_name', $form_data['employer-name'], $borrower_data);
        update_field('borrower_years_at_employer', $form_data['time-at-employer'], $borrower_data);
        update_field('borrower_months_at_employer', $form_data['months-at-employer'], $borrower_data);
        update_field('borrower_pay_frequency', $form_data['pay-frequency'], $borrower_data);
        update_field('borrower_way_you_find_us', $form_data['how-did-you-find-us'], $borrower_data);
        update_field('borrower_referred_by', $form_data['referred-by'], $borrower_data);

        $loan_result = loan_request(array(
            'campaign_code' => get_field('monevo_campaign_code', 'options'),
            'social_security_number' => $form_data['social-security-number'],
            'loan_purpose' => $form_data['loan-purpose'],
            'credit_rating' => $form_data['estimated-credit-score'],
            'education_level' => $form_data['highest-education-level'],
            'loan_amount' => $form_data['loan-amount'],
            'dob' => $form_data['date-of-birth'],
            'first_name' => urlencode($form_data['first-name']),
            'last_name' => urlencode($form_data['last-name']),
            'email' => $form_data['email'],
            'mobile_phone' => $form_data['primary-phone'],
            'best_call_time' => '3',
            'zipcode' => $form_data['borrower-address']['zip-postal-code'],
            'address1' => urlencode($form_data['borrower-address']['street-address']),
            'city' => urlencode($form_data['borrower-address']['city']),
            'state' => $form_data['borrower-address']['state-province'],
            'residence_type' => $form_data['residence-type'],
            'years_at_address' => $form_data['time-at-address'],
            'months_at_address' => $form_data['months-at-address']? $form_data['months-at-address'] : 0,
            'monthly_rent' => $form_data['monthly-payment'],
            'monthly_income' => $form_data['monthly-income'],
            'income_source' => $form_data['employment-status'],
            'employer_name' => !empty($form_data['employer-name']) ? urlencode($form_data['employer-name']) : 'none',
            'months_at_employer' => ($form_data['time-at-employer'] * 12) + $form_data['months-at-employer'],
            'pay_frequency' => !empty($form_data['pay-frequency']) ? $form_data['pay-frequency'] : '6',
            'terms_consent' => '1',
            'marketing_opt_in' => '1',
            'client_ip' => getUserIP(),
            'client_agent' => urlencode($_SERVER['HTTP_USER_AGENT']),
            'mode' => 'LIVE',
            'site_url' => 'itsfeeasy.com', // get_home_url();
            'site_ip' => '198.211.98.143', //$_SERVER['SERVER_ADDR']
            'military' => '0',
            'co_app' => $form_data['would-you-like-to-add-a-co-borrower'],
            'co_app_first_name' => urlencode($form_data['co-borrower-first-name']),
            'co_app_last_name' => urlencode($form_data['co-borrower-last-name']),
            'co_app_dob' => $form_data['co-borrower-date-of-birth'],
            'co_app_address1' => urlencode($form_data['co-borrower-address']['street-address']),
            'co_app_city' => urlencode($form_data['co-borrower-address']['city']),
            'co_app_state' => $form_data['co-borrower-address']['state-province'],
            'co_app_zipcode' => $form_data['co-borrower-address']['zip-postal-code'],
            'borrower_id' => $borrower_data
        ));


        $request_details = "";

        $request_details .= "<br><b>Loan Details</b><br>";
        $request_details .= "Loan Amount: ".$form_data['loan-amount']."<br>";
        $request_details .= "Loan Purpose: ".$form_data['loan-purpose']."<br>";
        $request_details .= "Estimated Credit Score: ".$form_data['estimated-credit-score']."<br>";

        $request_details .= "<br><b>Personal Details</b><br>";
        //$request_details .= "First Name: ".$form_data['first-name']."<br>";
        //$request_details .= "Last Name: ".$form_data['last-name']."<br>";
        //$request_details .= "DOB: ".$form_data['date-of-birth']."<br>";
        $request_details .= "Email: ".$form_data['email']."<br>";
        $request_details .= "Phone: ".$form_data['primary-phone']."<br>";
        //$request_details .= "Highest education level : ".$form_data['highest-education-level']."<br>";

        $request_details .= "<br><b>Address Details</b><br>";
        //$request_details .= "Address: ".$form_data['borrower-address']['street-address']."<br>";
        $request_details .= "City: ".$form_data['borrower-address']['city']."<br>";
        $request_details .= "State: ".$form_data['borrower-address']['state-province']."<br>";
        $request_details .= "ZIP code: ".$form_data['borrower-address']['zip-postal-code']."<br>";
        //$request_details .= "Residence Type: ".$form_data['residence-type']."<br>";
        //$request_details .= "Monthly Payment: ".$form_data['monthly-payment']."<br>";
        $request_details .= "Years at address: ".$form_data['time-at-address']."<br>";
        //$months_at_address = $form_data['months-at-address']? $form_data['months-at-address'] : '0';
        //$request_details .= "Months at address: ".$months_at_address."<br>";

        $request_details .= "<br><b>Employment Details</b><br>";
        $request_details .= "Employment Status: ".$form_data['employment-status']."<br>";
        $request_details .= "Monthly income: ".$form_data['monthly-income']."<br>";
        //$request_details .= "SSN: ".$form_data['social-security-number']."<br>";
        //$employer_name = !empty($form_data['employer-name']) ? urlencode($form_data['employer-name']) : 'none';
        //$request_details .= "Employer Name: ".$employer_name."<br>";
        $request_details .= "Years At Employer: ".$form_data['time-at-employer']."<br>";
        //$request_details .= "Months At Employer: ".$form_data['months-at-employer']."<br>";
        //$pay_frequency = !empty($form_data['pay-frequency']) ? $form_data['pay-frequency'] : '6';
        //$request_details .= "Pay frequency: ".$pay_frequency."<br>";

        $request_details .= "<br><b>Way you find us?</b><br>";
        $request_details .= "Way you find us: ".$form_data['how-did-you-find-us']."<br>";
        $request_details .= "Referred by: ".get_the_title($form_data['referred-by'])."<br>";

        $request_details .= "<br><b>Co-borrower Details</b><br>";
        $request_details .= "First Name: ".$form_data['co-borrower-first-name']."<br>";
        $request_details .= "Last Name: ".$form_data['co-borrower-last-name']."<br>";
        //$request_details .= "DOB: ".$form_data['co-borrower-date-of-birth']."<br>";
        //$request_details .= "Address: ".$form_data['co-borrower-address']['street-address']."<br>";
        $request_details .= "City: ".$form_data['co-borrower-address']['city']."<br>";
        $request_details .= "State: ".$form_data['co-borrower-address']['state-province']."<br>";
        $request_details .= "ZIP code: ".$form_data['co-borrower-address']['zip-postal-code']."<br>";
        $request_details .= "Annual Pre-tax Income: ".$form_data['co-borrower-annual-pre-tax-income']."<br>";

        $request_details .= "<br><b>Result</b><br>";
        $status = $loan_result['status'] == "20" ? "Accepted": "Rejected";
        $request_details .= "Status: ".$status."<br>";
        $request_details .= "Redirect Url: ".$loan_result['redirect_url']."<br>";



        //vendor notify
        $to = get_field('vendor_company_email', $form_data['referred-by']);
        $subject = get_field('loan_request_sent_to_vendor_subject', 'options');
        $body = get_field('loan_request_sent_to_vendor', 'options');
        $shortcodes = array("[vendor-login-link]");
        $values   = array("<a href='".get_field('vendor_login_page', 'options')."'>Login</a>");
        $body = str_replace($shortcodes, $values, $body);
        $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Feeasy <info@itsfeeasy.com>');
        wp_mail( $to, $subject, $body, $headers );

        //admin notify
        $to = get_option( 'admin_email' );
        $subject = get_field('loan_request_sent_to_admin_subject', 'options');
        $body = get_field('loan_request_sent_to_admin', 'options');
        $shortcodes = array("[admin-link]", "[borrower-name]", "[request-details]");
        $values   = array("<a href='https://itsfeeasy.com/me'>Login</a>", $form_data['first-name']." ".$form_data['last-name'], $request_details);
        $body = str_replace($shortcodes, $values, $body);
        $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Feeasy <info@itsfeeasy.com>');
        wp_mail( $to, $subject, $body, $headers );


        if ($loan_result['status'] == '20') {
            //borrower notify
            $to = $form_data['email'];
            $subject = get_field('loan_request_sent_get_offers_to_borrower_subject', 'options');
            $body = get_field('loan_request_sent_get_offers_to_borrower', 'options');
            $shortcodes = array("[borrower-first-name]", "[offers-link]");
            $values   = array($form_data['first-name'], "<a href='".$loan_result['redirect_url']."'>please click here</a>");
            $body = str_replace($shortcodes, $values, $body);
            $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Feeasy <info@itsfeeasy.com>');
            wp_mail( $to, $subject, $body, $headers );

            update_field('borrower_api_status', 'Accepted', $borrower_data);
            update_field('borrower_api_redirect_url', $loan_result['redirect_url'], $borrower_data);
            update_field('borrower_api_reference', $loan_result['reference'], $borrower_data);

            wp_redirect( $loan_result['redirect_url'] );
            exit;
        } else {
            //borrower notify
            $to = $form_data['email'];
            $subject = get_field('loan_request_sent_no_offers_to_borrower_subject', 'options');
            $body = get_field('loan_request_sent_no_offers_to_borrower', 'options');
            $shortcodes = array("[borrower-first-name]");
            $values   = array($form_data['first-name']);
            $body = str_replace($shortcodes, $values, $body);
            $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Feeasy <info@itsfeeasy.com>');
            wp_mail( $to, $subject, $body, $headers );

            update_field('borrower_api_status', 'Rejected', $borrower_data);
            if ($loan_result['redirect_url']){
                update_field('borrower_api_redirect_url', $loan_result['redirect_url'], $borrower_data);
            }
            wp_redirect( get_field('no_lender_found_page', 'options').'?bname='.$form_data['first-name'] );
            exit;
        }

        /*
        else if ($loan_result['status'] == '13') {

            //borrower notify
            $to = $form_data['email'];
            $subject = get_field('loan_request_sent_no_offers_to_borrower_subject', 'options');
            $body = get_field('loan_request_sent_no_offers_to_borrower', 'options');
            $shortcodes = array("[borrower-first-name]");
            $values   = array($form_data['first-name']);
            $body = str_replace($shortcodes, $values, $body);
            $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Feeasy <info@itsfeeasy.com>');
            wp_mail( $to, $subject, $body, $headers );

            update_field('borrower_api_status', 'Rejected', $borrower_data);
            update_field('borrower_api_redirect_url', $loan_result['redirect_url'], $borrower_data);
            update_field('borrower_api_reference', $loan_result['reference'], $borrower_data);
        } else if ($loan_result['status'] == '21' || $loan_result['status'] == '22') {

            //borrower notify
            $to = $form_data['email'];
            $subject = get_field('loan_request_sent_no_offers_to_borrower_subject', 'options');
            $body = get_field('loan_request_sent_no_offers_to_borrower', 'options');
            $shortcodes = array("[borrower-first-name]");
            $values   = array($form_data['first-name']);
            $body = str_replace($shortcodes, $values, $body);
            $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Feeasy <info@itsfeeasy.com>');
            wp_mail( $to, $subject, $body, $headers );

            update_field('borrower_api_status', 'No Lender Found', $borrower_data);
            update_field('borrower_api_redirect_url', get_field('no_lender_found_page','options'), $borrower_data);
            //update_field('borrower_api_reference', $loan_result['reference'], $borrower_data);
        } else if ($loan_result['status'] == '11' || $loan_result['status'] == '12') {
            //borrower notify
            $to = $form_data['email'];
            $subject = get_field('loan_request_sent_no_offers_to_borrower_subject', 'options');
            $body = get_field('loan_request_sent_no_offers_to_borrower', 'options');
            $shortcodes = array("[borrower-first-name]");
            $values   = array($form_data['first-name']);
            $body = str_replace($shortcodes, $values, $body);
            $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Feeasy <info@itsfeeasy.com>');
            wp_mail( $to, $subject, $body, $headers );

            update_field('borrower_api_status', 'Rejected (Invalid Data)', $borrower_data);
        } else if ($loan_result['status'] == '9') {
            update_field('borrower_api_status', 'API Error', $borrower_data);
        } else {
            update_field('borrower_api_status', 'Unknown API Status', $borrower_data);
        }
        */
    }
}

function get_borrowers()
{ ?>
    <div class="wrap">
        <h1 class="wp-heading-inline">Borrowers</h1>
        <table id="borrower-forms" class="display js-table" style="width:100%">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>State</th>
                <th>Loan Purpose</th>
                <th>Loan Amount</th>
                <th>Heard Of Us</th>
                <th>Referred By</th>
                <th>Result</th>
                <th>Offer Page Link</th>
                <th>Date Submitted</th>
                <th><span class="dashicons dashicons-edit"></span></th>
            </tr>
            </thead>
            <tbody>
            <?php $borrowers = get_posts(array(
                'numberposts' => -1,
                'post_type' => 'borrowers'
            ));

            foreach ($borrowers as $borrower) {
                ?>
                <tr>
                    <td><?php the_field('borrower_first_name', $borrower->ID); ?> <?php the_field('borrower_last_name', $borrower->ID); ?></td>
                    <td><?php the_field('borrower_email', $borrower->ID); ?></td>
                    <td><?php the_field('borrower_primary_phone', $borrower->ID); ?></td>
                    <td><?php the_field('borrower_state', $borrower->ID); ?></td>
                    <td><?php the_field('borrower_loan_purpose', $borrower->ID); ?></td>
                    <td><?php the_field('borrower_loan_amount', $borrower->ID); ?></td>
                    <td><?php the_field('borrower_way_you_find_us', $borrower->ID); ?></td>
                    <td><?php echo get_the_title(get_field('borrower_referred_by', $borrower->ID)); ?></td>
                    <td><?php the_field('borrower_api_status', $borrower->ID); ?></td>
                    <td>
                        <?php if (get_field('borrower_api_redirect_url', $borrower->ID)) { ?>
                            <a href="<?php the_field('borrower_api_redirect_url', $borrower->ID); ?>" target="_blank">Offer
                                Page Link</a>
                        <?php } ?>
                    </td>
                    <td><?php echo get_the_date('Y-m-d H:i:s', $borrower->ID); ?></td>
                    <td><a href="<?php echo get_edit_post_link($borrower->ID); ?>" title="Edit Borrower" target="_blank"
                           class="button action edit-btn"><span class="dashicons dashicons-edit"></span></a></td>
                </tr>
            <?php } ?>
            </tbody>
            <tfoot>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>State</th>
                <th>Loan Purpose</th>
                <th>Loan Amount</th>
                <th>Heard Of Us</th>
                <th>Referred By</th>
                <th>Result</th>
                <th>Redirect Url</th>
                <th>Created Date</th>
                <th><span class="dashicons dashicons-edit"></span></th>
            </tr>
            </tfoot>
        </table>
        <div class="csv-gen-btns">
            <a class="csv-gen" target="_blank" href="<?php the_field('borrowers_export_page', 'options'); ?>">Get Borrowers CSV</a>
        </div>
    </div>
<?php } ?>