/* Tables */
jQuery(document).ready(function ($) {

    if (document.querySelector('#vendor-forms')) {
        $('#vendor-forms').DataTable({
            ordering: true,
            search: true,
            "order": [[ 9, 'desc' ]]
        });
    }

    if (document.querySelector('#borrower-forms')) {
        $('#borrower-forms').DataTable({
            ordering: true,
            search: true,
            "order": [[ 10, 'desc' ]]
        });
    }

    if (document.querySelector('#leads-forms')) {
        $('#leads-forms').DataTable({
            ordering: true,
            search: true,
            "order": [[ 0, 'desc' ]]
        });
    }

});
