<?php /* Template Name: Vendor Login Page */
get_header();
?>

    <section class="vendor-login">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 login-note">
                    <?php the_field('form_text'); ?>
                </div>
                <div class="col-lg-6 login-form">
                    <?php if ($_GET['login'] == 'failed'){ ?>
                    <div class="form-error"><p>Invalid credentials.</p></div>
                    <?php
                    }

                    $label_username = get_field('user_label') ? get_field('user_label') : __('Email Address');
                    $label_password = get_field('password_label') ? get_field('password_label') : __('Password');
                    $label_remember = get_field('remember_me_label') ? get_field('remember_me_label') : __('Remember Me | ');
                    $label_log_in = get_field('login_label') ? get_field('login_label') : __('Log In');

                    $form = wp_login_form(array(
                        'echo' => false,
                        'redirect' => get_field('redirect_url'),
                        'form_id' => 'vendor_login_form',
                        'label_username' => '',
                        'label_password' => '',
                        'label_remember' => $label_remember,
                        'label_log_in' => $label_log_in,
                        'id_username' => 'user_login',
                        'id_password' => 'user_pass',
                        'id_remember' => 'rememberme',
                        'id_submit' => 'wp-submit',
                        'remember' => true,
                        'value_remember' => false,
                    ));

                    //add the placeholders
                    $form = str_replace('name="log"', 'name="log" placeholder="'.$label_username.'"', $form);
                    $form = str_replace('name="pwd"', 'name="pwd" placeholder="'.$label_password.'"', $form);

                    echo $form;

                    ?>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
?>