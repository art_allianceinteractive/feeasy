<?php /* Template Name: Get borrowers csv page */


if (!defined('DOING_AJAX') || !DOING_AJAX) {
    $user = wp_get_current_user();

    if (isset($user->roles) && is_array($user->roles)) {
        if (in_array('administrator', $user->roles)) {
            echo "";
        } else if (in_array('admin_lite', $user->roles)) {
            echo "";
        } else {
            wp_redirect(home_url());
            die;
        }
    }
}



$delimiter = ';';
$filename = "borrowers-" . date('Y-m-d H:i:s') . '.csv';

$array = array();

$borrowers = get_posts(array(
    'numberposts' => -1,
    'post_type' => 'borrowers',
    'order'     => 'DESC',
    'orderby'   => 'date'
));

foreach ($borrowers as $borrower) {
    $array[] = array(
        'First Name' => get_field('borrower_first_name', $borrower->ID),
        'Last Name' => get_field('borrower_last_name', $borrower->ID),
        'Email' => get_field('borrower_email', $borrower->ID),
        'Phone' => get_field('borrower_primary_phone', $borrower->ID),
        'State' => get_field('borrower_state', $borrower->ID),
        'Loan Purpose' => get_field('borrower_loan_purpose', $borrower->ID),
        'Loan Amount' => get_field('borrower_loan_amount', $borrower->ID),
        'Heard Of Us' => get_field('borrower_way_you_find_us', $borrower->ID),
        'Referred By' => get_field('borrower_referred_by', $borrower->ID) ? get_the_title(get_field('borrower_referred_by', $borrower->ID)) : '',
        'Result' => get_field('borrower_api_status', $borrower->ID),
        'Offer Page Link' => get_field('borrower_api_redirect_url', $borrower->ID),
        'Date Submitted' => get_the_date('Y-m-d H:i:s', $borrower->ID)
    );
}

header('Content-Type: application/csv');
header('Content-Disposition: attachment; filename="' . $filename . '";');

// clean output buffer
ob_end_clean();

$handle = fopen('php://output', 'w');

// use keys as column titles
fputcsv($handle, array_keys($array['0']));

foreach ($array as $value) {
    fputcsv($handle, $value, $delimiter);
}

fclose($handle);

// flush buffer
ob_flush();

// use exit to get rid of unexpected output afterward
exit();