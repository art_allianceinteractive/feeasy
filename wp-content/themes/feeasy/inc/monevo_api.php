<?php

function loan_request($attrs)
{

    $api_url = get_field('monevo_api_url', 'options');
    $request_url = "";
    $request_url .= $api_url;

    $attrs_count = 0;

    foreach ($attrs as $key => $value) {
        if ($key != 'borrower_id') {
            if ($attrs_count == 0) {
                $prefix = "?";
            } else {
                $prefix = "&";
            }

            if ($key == 'residence_type'){
                switch ($value) {
                    case "home_owner":
                        $value = "1";
                        break;
                    case "rent":
                        $value = "2";
                        break;
                    case "live_with_family":
                        $value = "3";
                        break;
                    case "other":
                        $value = "4";
                        break;
                }
            }

            if ($key == 'income_source'){
                switch ($value) {
                    case "employed":
                        $value = "1";
                        break;
                    case "retired":
                        $value = "3";
                        break;
                    case "self_employed":
                        $value = "5";
                        break;
                    case "student":
                        $value = "6";
                        break;
                    case "unemployed":
                        $value = "7";
                        break;
                }
            }


            if ($key == 'pay_frequency'){
                switch ($value) {
                    case "weekly":
                        $value = "1";
                        break;
                    case "every_two_weeks":
                        $value = "2";
                        break;
                    case "twice_a_month":
                        $value = "3";
                        break;
                    case "monthly":
                        $value = "4";
                        break;
                    case "not_applicable":
                        $value = "5";
                        break;
                    case "other":
                        $value = "6";
                        break;
                }
            }



            if ($key == 'loan_purpose'){
                switch ($value) {
                    case "debt_consolidation" :
                        $value = "1";
                        break;
                    case "home_improvement" :
                        $value = "2";
                        break;
                    case "home_staging":
                        $value = "5";
                        break;
                    case "tiny_home":
                        $value = "5";
                        break;
                    case "interior_design":
                        $value = "5";
                        break;
                    case "broker_commission":
                        $value = "5";
                        break;
                    case "moving_expenses":
                        $value = "5";
                        break;
                    case "rental_fees":
                        $value = "5";
                        break;
                    case "legal_fees":
                        $value = "5";
                        break;
                    case "other":
                        $value = "16";
                        break;
                }
            }


            if ($key == 'credit_rating'){
                switch ($value) {
                    case "poor":
                        $value = "1";
                        break;
                    case "fair":
                        $value = "2";
                        break;
                    case "good":
                        $value = "3";
                        break;
                    case "excellent":
                        $value = "4";
                        break;
                }
            }

            if ($key == 'education_level'){
                switch ($value) {
                    case "high_school":
                        $value = "1";
                        break;
                    case "associate":
                        $value = "2";
                        break;
                    case "bachelors":
                        $value = "3";
                        break;
                    case "masters":
                        $value = "4";
                        break;
                    case "other_graduate_degree":
                        $value = "5";
                        break;
                    case "other":
                        $value = "6";
                        break;
                }
            }

            //co_app_state
            if ($key == 'state' || $key == 'co_app_state'){
                switch ($value) {
                    case "Alaska":
                        $value = "AK";
                        break;
                    case "Alabama":
                        $value = "AL";
                        break;
                    case "Arkansas":
                        $value = "AR";
                        break;
                    case "Arizona":
                        $value = "AZ";
                        break;
                    case "California":
                        $value = "CA";
                        break;
                    case "Colorado":
                        $value = "CO";
                        break;
                    case "Connecticut":
                        $value = "CT";
                        break;
                    case "District of Columbia":
                        $value = "DC";
                        break;
                    case "Delaware":
                        $value = "DE";
                        break;
                    case "Florida":
                        $value = "FL";
                        break;
                    case "Georgia":
                        $value = "GA";
                        break;
                    case "Hawaii":
                        $value = "HI";
                        break;
                    case "Iowa":
                        $value = "IA";
                        break;
                    case "Idaho":
                        $value = "ID";
                        break;
                    case "Illinois":
                        $value = "IL";
                        break;
                    case "Indiana":
                        $value = "IN";
                        break;
                    case "Kansas":
                        $value = "KS";
                        break;
                    case "Kentucky":
                        $value = "KY";
                        break;
                    case "Louisiana":
                        $value = "LA";
                        break;
                    case "Massachusetts":
                        $value = "MA";
                        break;
                    case "Maryland":
                        $value = "MD";
                        break;
                    case "Maine":
                        $value = "ME";
                        break;
                    case "Michigan":
                        $value = "MI";
                        break;
                    case "Minnesota":
                        $value = "MN";
                        break;
                    case "Missouri":
                        $value = "MO";
                        break;
                    case "Mississippi":
                        $value = "MS";
                        break;
                    case "Montana":
                        $value = "MT";
                        break;
                    case "North Carolina":
                        $value = "NC";
                        break;
                    case "North Dakota":
                        $value = "ND";
                        break;
                    case "Nebraska":
                        $value = "NE";
                        break;
                    case "New Hampshire":
                        $value = "NH";
                        break;
                    case "New Jersey":
                        $value = "NJ";
                        break;
                    case "New Mexico":
                        $value = "NM";
                        break;
                    case "Nevada":
                        $value = "NV";
                        break;
                    case "New York":
                        $value = "NY";
                        break;
                    case "Ohio":
                        $value = "OH";
                        break;
                    case "Oklahoma":
                        $value = "OK";
                        break;
                    case "Oregon":
                        $value = "OR";
                        break;
                    case "Pennsylvania":
                        $value = "PA";
                        break;
                    case "Puerto Rico":
                        $value = "PR";
                        break;
                    case "Rhode Island":
                        $value = "RI";
                        break;
                    case "South Carolina":
                        $value = "SC";
                        break;
                    case "South Dakota":
                        $value = "SD";
                        break;
                    case "Tennessee":
                        $value = "TN";
                        break;
                    case "Texas":
                        $value = "TX";
                        break;
                    case "Utah":
                        $value = "UT";
                        break;
                    case "Virginia":
                        $value = "VA";
                        break;
                    case "Vermont":
                        $value = "VT";
                        break;
                    case "Washington":
                        $value = "WA";
                        break;
                    case "Wisconsin":
                        $value = "WI";
                        break;
                    case "West Virginia":
                        $value = "WV";
                        break;
                    case "Wyoming":
                        $value = "WY";
                        break;
                }
            }

            if ($key == 'mobile_phone'){
                $value = preg_replace('/[^0-9]/', '', $value);
            }

            if ($key == 'social_security_number'){
                $value = preg_replace('/[^0-9]/', '', $value);
            }

            $request_url .= $prefix . $key . "=" . $value;
            $attrs_count++;

        }
    }

    //$request_url = "https://usa.monevo.com/api?campaign_code=cF1bbwMGCwdHTH4&social_security_number=451803526&loan_purpose=1&credit_rating=1&education_level=1&monthly_rent=200&loan_amount=350&dob=1959-04-06&first_name=Thomas&last_name=Santos&email=batman@batcave.com&mobile_phone=9176530352&best_call_time=3&zipcode=19901&address1=615&city=Dover&state=DE&residence_type=2&years_at_address=5&months_at_address=30&income_source=5&employer_name=Test&months_at_employer=4&pay_frequency=2&terms_consent=1&marketing_opt_in=1&client_ip=199.47.74.66&client_agent=Mozilla%2F5.0+%28iPhone%3B+CPU+iPhone+OS+7_1+like+Mac+OS+X%29+AppleWebKit%2F537.51.2+%28KHTML%2C+like+Gecko%29+Version%2F7.0+Mobile%2F11D167+Safari%2F9537.53&country=USA&mode=LIVE&monthly_income=875&site_url=https://itsfeeasy.com/&military=0";
    //$attrs['borrower_id'] = '12345';


    $ch = curl_init($request_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    $data = curl_exec($ch);


    $return_arr = array();
    $response = '';


    if (simplexml_load_string($data) === false) {
        $return_arr = array(
            'status' => 9
        );

        $response = " status code: 9 \n error description: API response parse error \n";
    } else {
        $xml = simplexml_load_string($data);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);

        $return_arr = array(
            'status' => $array['result'],
            'redirect_url' => $array['redirect_url'],
            'reference' => $array['reference']
        );

        $response = " status code: ".$xml->result." \n redirect_url: ".$xml->redirect_url." \n reference: ".$xml->reference." \n message: ".$xml->message." \n error code: ".$xml->error->code." \n error description: ".$xml->error->desc." \n";
    }

    $log = get_field('monevo_loan_requests_log', 'options');
    $new_log = "\n\n============= loan request ID: " . $attrs['borrower_id'] . " (" . date('Y-m-d h:i:sa') . ") ============= \n" . "=== Request === \n" . $request_url . "\n" . "=== Response === \n" . $response;
    update_field( $new_log . $log, 'options');
    update_field('borrower_api_log', $new_log, $attrs['borrower_id']);

    return $return_arr;

}