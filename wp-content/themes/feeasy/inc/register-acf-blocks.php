<?php

function feeasy_acf_blocks()
{

    if (!function_exists('acf_register_block_type')) return;
    $blocks = scandir(get_template_directory() . '/template-parts/acf-blocks', 1);
    foreach ($blocks as $block) {
        if ($block != '.' && $block != '..') {
            include_once get_template_directory() . '/template-parts/acf-blocks/'.$block.'/register.php';
        }
    }
}

function feeasy_allowed_block_types($allowed_blocks)
{
    //$allowed_blocks = array();
    $blocks = scandir(get_template_directory() . '/template-parts/acf-blocks', 1);
    foreach ($blocks as $block) {
        if ($block != '.' && $block != '..') {
            $allowed_blocks[] = 'acf/'.$block;
        }
    }
    return $allowed_blocks;
}

function feeasy_block_categories( $categories) {
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'feeasy',
                'title' => __( 'Feeasy', 'feeasy' ),
                'icon'  => null,
            ),
        )
    );
}

add_action('acf/init', 'feeasy_acf_blocks');
add_filter('allowed_block_types', 'feeasy_allowed_block_types');
add_filter('block_categories', 'feeasy_block_categories', 10, 2 );