<?php /* Template Name: Get leads csv page */


if (!defined('DOING_AJAX') || !DOING_AJAX) {
    $user = wp_get_current_user();

    if (isset($user->roles) && is_array($user->roles)) {
        if (in_array('administrator', $user->roles)) {
            echo "";
        } else if (in_array('admin_lite', $user->roles)) {
            echo "";
        } else {
            wp_redirect(home_url());
            die;
        }
    }
}

$delimiter = ';';
$filename = "leads-" . date('Y-m-d H:i:s') . '.csv';

$array = array();

$leads = get_posts(array(
    'numberposts' => -1,
    'post_type' => 'leads',
    'order'     => 'DESC',
    'orderby'   => 'date'
));

foreach ($leads as $lead) {
    $array[] = array(
        'Submitted' => get_the_date('Y-m-d H:i:s', $lead->ID),
        'Client First Name' => get_field('client_first_name', $lead->ID),
        'Client Last Name' => get_field('client_last_name', $lead->ID),
        'Client Phone' => get_field('client_phone_number', $lead->ID),
        'Client Email' => get_field('client_email_address', $lead->ID),
        'Loan Amount' => get_field('client_loan_amount', $lead->ID),
        'Agent First Name' => get_field('agent_first_name', $lead->ID),
        'Agent Last Name' => get_field('agent_last_name', $lead->ID),
        'Agent Phone' => get_field('agent_phone_number', $lead->ID),
        'Agent Email' => get_field('agent_email_address', $lead->ID),
        'Agent City' => get_field('agent_city', $lead->ID),
        'Agent State' => get_field('agent_state', $lead->ID),
        'Agent Company' => get_field('agent_company', $lead->ID),
        'Message' => get_field('agent_message', $lead->ID),
        'Contacted?' => get_field('client_contacted', $lead->ID),
        'Admin Note' => get_field('admin_note', $lead->ID)
    );
}

header('Content-Type: application/csv');
header('Content-Disposition: attachment; filename="' . $filename . '";');

// clean output buffer
ob_end_clean();

$handle = fopen('php://output', 'w');

// use keys as column titles
fputcsv($handle, array_keys($array['0']));

foreach ($array as $value) {
    fputcsv($handle, $value, $delimiter);
}

fclose($handle);

// flush buffer
ob_flush();

// use exit to get rid of unexpected output afterward
exit();