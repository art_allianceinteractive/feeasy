<?php /* Template Name: Get vendors csv page */


if (!defined('DOING_AJAX') || !DOING_AJAX) {
    $user = wp_get_current_user();

    if (isset($user->roles) && is_array($user->roles)) {
        if (in_array('administrator', $user->roles)) {
            echo "";
        } else if (in_array('admin_lite', $user->roles)) {
            echo "";
        } else {
            wp_redirect(home_url());
            die;
        }
    }
}



$delimiter = ';';
$filename = "vendors-" . date('Y-m-d H:i:s') . '.csv';

$array = array();

$vendors = get_posts(array(
    'numberposts' => -1,
    'post_type' => 'vendors',
    'order'     => 'DESC',
    'orderby'   => 'date'
));

foreach ($vendors as $vendor) {
    $array[] = array(
        'First Name' => get_field('vendor_first_name', $vendor->ID),
        'Last Name' => get_field('vendor_last_name', $vendor->ID),
        'Email' => get_field('vendor_company_email', $vendor->ID),
        'Phone' => get_field('vendor_company_phone', $vendor->ID),
        'Company Name' => get_field('vendor_company_name', $vendor->ID),
        'Company Website' => get_field('vendor_company_website', $vendor->ID),
        'Company Phone' => get_field('vendor_company_phone', $vendor->ID),
        'City' => get_field('vendor_city', $vendor->ID),
        'State' => get_field('vendor_state', $vendor->ID),
        'Referral URL' => get_field('vendor_application_page','options')."?vendor_id=".$vendor->ID,
        'Created Date' => get_the_date('Y-m-d', $vendor->ID)
    );
}

header('Content-Type: application/csv');
header('Content-Disposition: attachment; filename="' . $filename . '";');

// clean output buffer
ob_end_clean();

$handle = fopen('php://output', 'w');

// use keys as column titles
fputcsv($handle, array_keys($array['0']));

foreach ($array as $value) {
    fputcsv($handle, $value, $delimiter);
}

fclose($handle);

// flush buffer
ob_flush();

// use exit to get rid of unexpected output afterward
exit();