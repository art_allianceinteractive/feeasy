<div class="basic-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 nav-wrapper">
                <?php
                wp_nav_menu( array(
                    'theme_location'    => "footer-menu-top",
                    'menu_class'   => "txt-light-blue txt-sm-md",
                    'item_spacing'      => "discard"
                ) );
                wp_nav_menu( array(
                    'theme_location'    => "footer-menu-bottom",
                    'menu_class'   => "txt-light-blue txt-sm-md",
                    'item_spacing'      => "discard"
                ) );
                ?>

                <ul class="social-links">
                    <?php if (get_field('twitter', 'options')){?>
                    <li>
                        <a href="<?php the_field('twitter', 'options'); ?>" target="_blank" rel="nofollow">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                                <path fill="#000000" d="M16 3.538c-0.588 0.263-1.222 0.438-1.884 0.516 0.678-0.406 1.197-1.050 1.444-1.816-0.634 0.375-1.338 0.65-2.084 0.797-0.6-0.638-1.453-1.034-2.397-1.034-1.813 0-3.281 1.469-3.281 3.281 0 0.256 0.028 0.506 0.084 0.747-2.728-0.138-5.147-1.444-6.766-3.431-0.281 0.484-0.444 1.050-0.444 1.65 0 1.138 0.578 2.144 1.459 2.731-0.538-0.016-1.044-0.166-1.488-0.409 0 0.013 0 0.028 0 0.041 0 1.591 1.131 2.919 2.634 3.219-0.275 0.075-0.566 0.116-0.866 0.116-0.212 0-0.416-0.022-0.619-0.059 0.419 1.303 1.631 2.253 3.066 2.281-1.125 0.881-2.538 1.406-4.078 1.406-0.266 0-0.525-0.016-0.784-0.047 1.456 0.934 3.181 1.475 5.034 1.475 6.037 0 9.341-5.003 9.341-9.341 0-0.144-0.003-0.284-0.009-0.425 0.641-0.459 1.197-1.038 1.637-1.697z"></path>
                            </svg>
                        </a>
                    </li>
                    <?php } ?>
                    <?php if (get_field('facebook', 'options')){?>
                    <li>
                        <a href="<?php the_field('facebook', 'options'); ?>" target="_blank" rel="nofollow">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                                <path fill="#000000" d="M9.5 3h2.5v-3h-2.5c-1.93 0-3.5 1.57-3.5 3.5v1.5h-2v3h2v8h3v-8h2.5l0.5-3h-3v-1.5c0-0.271 0.229-0.5 0.5-0.5z"></path>
                            </svg>
                        </a>
                    </li>
                    <?php } ?>
                    <?php if (get_field('instagram', 'options')){?>
                    <li>
                        <a href="<?php the_field('instagram', 'options'); ?>" target="_blank" rel="nofollow">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                                <path fill="#000000" d="M14.5 0h-13c-0.825 0-1.5 0.675-1.5 1.5v13c0 0.825 0.675 1.5 1.5 1.5h13c0.825 0 1.5-0.675 1.5-1.5v-13c0-0.825-0.675-1.5-1.5-1.5zM11 2.5c0-0.275 0.225-0.5 0.5-0.5h2c0.275 0 0.5 0.225 0.5 0.5v2c0 0.275-0.225 0.5-0.5 0.5h-2c-0.275 0-0.5-0.225-0.5-0.5v-2zM8 5c1.656 0 3 1.344 3 3s-1.344 3-3 3c-1.656 0-3-1.344-3-3s1.344-3 3-3zM14 13.5v0c0 0.275-0.225 0.5-0.5 0.5h-11c-0.275 0-0.5-0.225-0.5-0.5v0-6.5h1.1c-0.066 0.322-0.1 0.656-0.1 1 0 2.762 2.237 5 5 5s5-2.238 5-5c0-0.344-0.034-0.678-0.1-1h1.1v6.5z"></path>
                            </svg>
                        </a>
                    </li>
                    <?php } ?>
                    <?php if (get_field('linkedin', 'options')){?>
                    <li>
                        <a href="<?php the_field('linkedin', 'options'); ?>" target="_blank" rel="nofollow">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                                <path fill="#000000" d="M6 6h2.767v1.418h0.040c0.385-0.691 1.327-1.418 2.732-1.418 2.921 0 3.461 1.818 3.461 4.183v4.817h-2.885v-4.27c0-1.018-0.021-2.329-1.5-2.329-1.502 0-1.732 1.109-1.732 2.255v4.344h-2.883v-9z"></path>
                                <path fill="#000000" d="M1 6h3v9h-3v-9z"></path>
                                <path fill="#000000" d="M4 3.5c0 0.828-0.672 1.5-1.5 1.5s-1.5-0.672-1.5-1.5c0-0.828 0.672-1.5 1.5-1.5s1.5 0.672 1.5 1.5z"></path>
                            </svg>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
                <span class="copyright-text txt-light-blue txt-sm-sm-md"><?php the_field('copyright_text', 'options')?></span>
            </div>
        </div>
    </div>
</div>