<style media="all" type="text/css">

    /* background */
    <?php if (get_field('background_type')){ ?>
    .news.<?php echo $block['id']; ?> {
    <?php if (get_field('background_type') == 'bg_image'){ ?> background-image: url("<?php echo wp_get_attachment_image_src(get_field('background_image'), 'full')[0]; ?>");
    <?php } else if (get_field('background_type') == 'bg_color'){?> background-color: <?php echo get_field('background_color'); ?>;
    <?php } ?>
    }

    <?php } ?>

    /* xs & sm (mobile) */
    @media (max-width: 767.98px) {
    <?php if (!get_field('show_on_mobile')){ ?>
        .news.<?php echo $block['id']; ?> {
            display: none;
        }

    <?php } else { ?>
        .news.<?php echo $block['id']; ?> {
            padding-top: <?php echo get_field('m_padding_top').'px'; ?>;
            padding-bottom: <?php echo get_field('m_padding_bottom').'px'; ?>;
            margin-top: <?php echo get_field('m_margin_top').'px'; ?>;
            margin-bottom: <?php echo get_field('m_margin_bottom').'px'; ?>;
        }

    <?php } ?>
    }

    /* md (tablet) */
    @media (min-width: 768px) and (max-width: 991.98px) {
    <?php if (!get_field('show_on_tablet')){ ?>
        .news.<?php echo $block['id']; ?> {
            display: none;
        }

    <?php } else { ?>
        .news.<?php echo $block['id']; ?> {
            padding-top: <?php echo get_field('t_padding_top').'px'; ?>;
            padding-bottom: <?php echo get_field('t_padding_bottom').'px'; ?>;
            margin-top: <?php echo get_field('t_margin_top').'px'; ?>;
            margin-bottom: <?php echo get_field('t_margin_bottom').'px'; ?>;
        }

    <?php } ?>
    }

    /* lg (desktop) */
    @media (min-width: 992px) {
    <?php if (!get_field('show_on_desktop')){ ?>
        .news.<?php echo $block['id']; ?> {
            display: none;
        }

    <?php } else { ?>
        .news.<?php echo $block['id']; ?> {
            padding-top: <?php echo get_field('d_padding_top').'px'; ?>;
            padding-bottom: <?php echo get_field('d_padding_bottom').'px'; ?>;
            margin-top: <?php echo get_field('d_margin_top').'px'; ?>;
            margin-bottom: <?php echo get_field('d_margin_bottom').'px'; ?>;
        }

    <?php } ?>
    }

    /* custom styles */
    <?php echo get_field('custom_styles'); ?>

</style>

<?php
$htag1 = '<' . get_field('h_tag') . ' class="txt-sm txt-gray section-head">';
$htag2 = '</' . get_field('h_tag') . '>';
?>
<section class="news <?php echo $block['id'] . ' ' . $block['className']; ?>" id="<?php echo $block['id']; ?>">
    <div class="<?php the_field('section_width'); ?>">
        <div class="row">
            <div class="col-sm-12">
                <?php echo $htag1 . get_field('heading') . $htag2; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-6 featured-wrapper">
                <a href="<?php echo get_the_permalink(get_field('featured_news_post')); ?>">
                    <?php echo get_the_post_thumbnail(get_field('featured_news_post'), 'full', array('alt'=>get_field('heading'))); ?>
                    <h3 class="txt-xl"><?php echo get_the_title(get_field('featured_news_post')); ?></h3>
                    <span class="txt-md"><?php echo get_the_excerpt(get_field('featured_news_post')); ?></span>
                </a>
            </div>
            <div class="col-sm-12 col-md-6 latest-wrapper">
                <ul>
                    <?php $latest_posts = get_posts(array(
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'exclude' => get_field('featured_news_post'),
                        'numberposts' => 3
                    ));

                    foreach ($latest_posts as $post) { ?>
                        <li>
                            <a href="<?php echo get_the_permalink($post->ID) ?>">
                                <h3 class="txt-lg"><?php echo get_the_title($post->ID); ?></h3>
                                <span class="txt-md"><?php echo get_the_excerpt($post->ID); ?></span>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
                <?php $btn_count = 0;
                if (have_rows('buttons')) { ?>
                    <div class="buttons-wrapper">
                        <?php while (have_rows('buttons')) {
                            the_row(); ?>
                            <style>
                                .rte.<?php echo $block['id']; ?> .buttons-wrapper {
                                    text-align: <?php the_sub_field('aligment'); ?>;
                                }
                            </style>
                            <a href="<?php echo get_sub_field('button')['url']; ?>" <?php echo get_sub_field('button')['target'] == '_blank' ? 'target="_blank"' : ''; ?>
                               class="btn btn-primary btn-<?php echo $btn_count; ?>"><?php echo get_sub_field('button')['title']; ?></a>
                            <?php $btn_count++;
                        } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>