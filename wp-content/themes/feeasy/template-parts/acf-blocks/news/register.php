<?php
$section_name = 'news';
    acf_register_block_type(array(
    'name' => $section_name,
    'title' => __('News', 'feeasy'),
    'description' => __('news section', 'feeasy'),
    'category' => 'feeasy', // common | formatting | layout | widgets | embed
    'icon' => 'layout',
    'keywords' => array('news', 'text', 'mce', 'simple'),
    'post_types' => array('page', 'post'),
    'mode' => 'edit', // preview | edit | auto
    'align' => 'full', // left | center | right | wide | full
    'render_template' => get_template_directory() . '/template-parts/acf-blocks/'.$section_name.'/template.php',
    'enqueue_assets' => function(){
        wp_enqueue_style( 'news', get_template_directory_uri() . '/template-parts/acf-blocks/news/styles.css' );
        wp_enqueue_script( 'news', get_template_directory_uri() . '/template-parts/acf-blocks/news/scripts.js', array('jquery'), '', true );
    },
    'supports' => array(
        'align' => false, // array( 'left', 'right', 'full' ),
        'multiple' => true, // ture | false
        'mode' => false, // true | false
    ),
));

// Register Block Fields Group
include_once 'acf_field_group.php';