<style media="all" type="text/css">

    /* background */
    <?php if (get_field('background_type')){ ?>
    .team.<?php echo $block['id']; ?> {
    <?php if (get_field('background_type') == 'bg_image'){ ?> background-image: url("<?php echo wp_get_attachment_image_src(get_field('background_image'), 'full')[0]; ?>");
    <?php } else if (get_field('background_type') == 'bg_color'){?> background-color: <?php echo get_field('background_color'); ?>;
    <?php } ?>
    }

    <?php } ?>

    /* xs & sm (mobile) */
    @media (max-width: 767.98px) {
    <?php if (!get_field('show_on_mobile')){ ?>
        .team.<?php echo $block['id']; ?> {
            display: none;
        }

    <?php } else { ?>
        .team.<?php echo $block['id']; ?> {
            padding-top: <?php echo get_field('m_padding_top').'px'; ?>;
            padding-bottom: <?php echo get_field('m_padding_bottom').'px'; ?>;
            margin-top: <?php echo get_field('m_margin_top').'px'; ?>;
            margin-bottom: <?php echo get_field('m_margin_bottom').'px'; ?>;
        }

    <?php } ?>
    }

    /* md (tablet) */
    @media (min-width: 768px) and (max-width: 991.98px) {
    <?php if (!get_field('show_on_tablet')){ ?>
        .team.<?php echo $block['id']; ?> {
            display: none;
        }

    <?php } else { ?>
        .team.<?php echo $block['id']; ?> {
            padding-top: <?php echo get_field('t_padding_top').'px'; ?>;
            padding-bottom: <?php echo get_field('t_padding_bottom').'px'; ?>;
            margin-top: <?php echo get_field('t_margin_top').'px'; ?>;
            margin-bottom: <?php echo get_field('t_margin_bottom').'px'; ?>;
        }

    <?php } ?>
    }

    /* lg (desktop) */
    @media (min-width: 992px) {
    <?php if (!get_field('show_on_desktop')){ ?>
        .team.<?php echo $block['id']; ?> {
            display: none;
        }

    <?php } else { ?>
        .team.<?php echo $block['id']; ?> {
            padding-top: <?php echo get_field('d_padding_top').'px'; ?>;
            padding-bottom: <?php echo get_field('d_padding_bottom').'px'; ?>;
            margin-top: <?php echo get_field('d_margin_top').'px'; ?>;
            margin-bottom: <?php echo get_field('d_margin_bottom').'px'; ?>;
        }

    <?php } ?>
    }

    /* custom styles */
    <?php echo get_field('custom_styles'); ?>

</style>
<section class="team <?php echo $block['id'] . ' ' . $block['className']; ?>" id="<?php echo $block['id']; ?>">
    <div class="<?php the_field('section_width'); ?>">
        <?php if (get_field('heading')) {
            $htag1 = '<' . get_field('h_tag') . ' class="txt-xl txt-dark-blue section-head">';
            $htag2 = '</' . get_field('h_tag') . '>';
            ?>
            <div class="row">
                <div class="col-12">
                    <?php echo $htag1 . get_field('heading') . $htag2; ?>
                </div>
            </div>
        <?php } ?>
        <div class="row">
            <?php $bio_count = 0;
            foreach (get_field('team_members') as $bio) { ?>
                <div class="col-sm-<?php the_field('m_grid'); ?> col-md-<?php the_field('t_grid'); ?> col-lg-<?php the_field('d_grid'); ?> bio-wrapper bio-<?php echo $bio_count; ?>">
                    <div class="bio-photo">
                        <?php //echo get_the_post_thumbnail($bio->ID, 'full'); ?>
                    </div>
                    <div class="bio-details">
                        <b><?php echo $bio->post_title; ?></b>
                        <span class="txt-sm-md"><?php the_field('position', $bio->ID); ?></span>
                    </div>
                </div>
                <?php $bio_count++;
            } ?>
        </div>
    </div>
</section>