<style media="all" type="text/css">

    /* general */
    <?php if (get_field('background_type')){ ?>
    .gravity-form.<?php echo $block['id']; ?> {
    <?php if (get_field('background_type') == 'bg_image'){ ?> background-image: url("<?php echo wp_get_attachment_image_src(get_field('background_image'), 'full')[0]; ?>");
    <?php } else if (get_field('background_type') == 'bg_color'){?> background-color: <?php echo get_field('background_color'); ?>;
    <?php } ?>
    }

    <?php } ?>

    .gravity-form.<?php echo $block['id']; ?>,
    .gravity-form.<?php echo $block['id']; ?> p {
        color: <?php the_field('text_color'); ?>;
    }

    /* xs & sm (mobile) */
    @media (max-width: 767.98px) {
    <?php if (!get_field('show_on_mobile')){ ?>
        .gravity-form.<?php echo $block['id']; ?> {
            display: none;
        }

    <?php } else { ?>
        .gravity-form.<?php echo $block['id']; ?> {
            padding-top: <?php echo get_field('m_padding_top').'px'; ?>;
            padding-bottom: <?php echo get_field('m_padding_bottom').'px'; ?>;
            margin-top: <?php echo get_field('m_margin_top').'px'; ?>;
            margin-bottom: <?php echo get_field('m_margin_bottom').'px'; ?>;
        }

    <?php } ?>
    }

    /* md (tablet) */
    @media (min-width: 768px) and (max-width: 991.98px) {
    <?php if (!get_field('show_on_tablet')){ ?>
        .gravity-form.<?php echo $block['id']; ?> {
            display: none;
        }

    <?php } else { ?>
        .gravity-form.<?php echo $block['id']; ?> {
            padding-top: <?php echo get_field('t_padding_top').'px'; ?>;
            padding-bottom: <?php echo get_field('t_padding_bottom').'px'; ?>;
            margin-top: <?php echo get_field('t_margin_top').'px'; ?>;
            margin-bottom: <?php echo get_field('t_margin_bottom').'px'; ?>;
        }

    <?php } ?>
    }

    /* lg (desktop) */
    @media (min-width: 992px) {
    <?php if (!get_field('show_on_desktop')){ ?>
        .gravity-form.<?php echo $block['id']; ?> {
            display: none;
        }

    <?php } else { ?>
        .gravity-form.<?php echo $block['id']; ?> {
            padding-top: <?php echo get_field('d_padding_top').'px'; ?>;
            padding-bottom: <?php echo get_field('d_padding_bottom').'px'; ?>;
            margin-top: <?php echo get_field('d_margin_top').'px'; ?>;
            margin-bottom: <?php echo get_field('d_margin_bottom').'px'; ?>;
        }

    <?php } ?>
    }

    /* custom styles */
    <?php echo get_field('custom_styles'); ?>

</style>
<section class="gravity-form <?php echo $block['id'] . ' ' . $block['className']; ?>" id="<?php echo $block['id']; ?>">
    <div class="<?php the_field('section_width'); ?>">
        <?php if (get_field('heading')) {
            $htag1 = '<' . get_field('h_tag') . ' class="txt-xxl txt-white section-head">';
            $htag2 = '</' . get_field('h_tag') . '>';
            ?>
            <div class="row">
                <div class="col-12">
                    <?php echo $htag1 . get_field('heading') . $htag2; ?>
                </div>
            </div>
        <?php } ?>
        <div class="row form-wrapper">
            <div class="col-sm-<?php the_field('m_grid'); ?> col-md-<?php the_field('t_grid'); ?> col-lg-<?php the_field('d_grid'); ?>">

                <?php
                if (get_field('gform') == get_field('application_form', 'options')) {
                    if (FALSE === get_post_status($_GET['vendor_id'])) {
                        $values = false;
                    } else {
                        if (get_field('vendor_login_user', $_GET['vendor_id'])) {
                            $values = array(
                                'referred-by' => $_GET['vendor_id'],
                                'referred-by-name' => get_field('vendor_first_name', $_GET['vendor_id']) . " " . get_field('vendor_last_name', $_GET['vendor_id'])
                            );
                        } else {
                            $values = false;
                        }
                    }
                } else {
                    $values = false;
                }
                ?>

                <?php gravity_form(get_field('gform'), false, false, false, $values, get_field('gform_ajax'), 1, $echo = true); ?>
            </div>
        </div>
    </div>
</section>