<?php

$section_name = 'gravity-form';
    acf_register_block_type(array(
    'name' => $section_name,
    'title' => __('Gravity Form', 'wombat'),
    'description' => __('Gravity Form', 'wombat'),
    'category' => 'wombat', // common | formatting | layout | widgets | embed
    'icon' => 'layout',
    'keywords' => array('gravity-form', 'text', 'mce', 'simple'),
    'post_types' => array('page', 'post'),
    'mode' => 'edit', // preview | edit | auto
    'align' => 'full', // left | center | right | wide | full
    'render_template' => get_template_directory() . '/template-parts/acf-blocks/'.$section_name.'/template.php',
    'enqueue_assets' => function(){
        wp_enqueue_style( 'gravity-form', get_template_directory_uri() . '/template-parts/acf-blocks/gravity-form/styles.css' );
        wp_enqueue_script( 'gravity-form', get_template_directory_uri() . '/template-parts/acf-blocks/gravity-form/scripts.js', array('jquery'), '', true );
    },
    'supports' => array(
        'align' => false, // array( 'left', 'right', 'full' ),
        'multiple' => true, // ture | false
        'mode' => false, // true | false
    ),
));

// Register Block Fields Group
include_once 'acf_field_group.php';