<?php
$section_name = 'text-boxes';
    acf_register_block_type(array(
    'name' => $section_name,
    'title' => __('Text Boxes', 'feeasy'),
    'description' => __('Text Boxes Grid', 'feeasy'),
    'category' => 'feeasy', // common | formatting | layout | widgets | embed
    'icon' => 'layout',
    'keywords' => array('text', 'boxes', 'text', 'mce', 'simple', 'grid'),
    'post_types' => array('page', 'post'),
    'mode' => 'edit', // preview | edit | auto
    'align' => 'full', // left | center | right | wide | full
    'render_template' => get_template_directory() . '/template-parts/acf-blocks/'.$section_name.'/template.php',
    'enqueue_assets' => function(){
        wp_enqueue_style( 'text-boxes', get_template_directory_uri() . '/template-parts/acf-blocks/text-boxes/styles.css' );
        wp_enqueue_script( 'text-boxes', get_template_directory_uri() . '/template-parts/acf-blocks/text-boxes/scripts.js', array('jquery'), '', true );
    },
    'supports' => array(
        'align' => false, // array( 'left', 'right', 'full' ),
        'multiple' => true, // ture | false
        'mode' => false, // true | false
    ),
));

// Register Block Fields Group
include_once 'acf_field_group.php';