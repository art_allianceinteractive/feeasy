<style media="all" type="text/css">

    <?php if (get_field('add_box_shadow')){ ?>
    .text-boxes.<?php echo $block['id']; ?> .text-box {
        -webkit-box-shadow: 0 0 20px 0 rgba(100, 100, 100, .5);
        box-shadow: 0 0 20px 0 rgba(100, 100, 100, .5);
        border-radius: 5px;
        padding: 30px;
        background-color: #FFF;
    }
    <?php } ?>

    /* xs & sm (mobile) */
    @media (max-width: 767.98px) {
    <?php if (!get_field('show_on_mobile')){ ?>
        .text-boxes.<?php echo $block['id']; ?> {
            display: none;
        }
    <?php } else { ?>
        .text-boxes.<?php echo $block['id']; ?> {
            padding-top: <?php echo get_field('m_padding_top').'px'; ?>;
            padding-bottom: <?php echo get_field('m_padding_bottom').'px'; ?>;
            margin-top: <?php echo get_field('m_margin_top').'px'; ?>;
            margin-bottom: <?php echo get_field('m_margin_bottom').'px'; ?>;
        }
    <?php } ?>
    }
    /* md (tablet) */
    @media (min-width: 768px) and (max-width: 991.98px) {
    <?php if (!get_field('show_on_tablet')){ ?>
        .text-boxes.<?php echo $block['id']; ?> {
            display: none;
        }
    <?php } else { ?>
        .text-boxes.<?php echo $block['id']; ?> {
            padding-top: <?php echo get_field('t_padding_top').'px'; ?>;
            padding-bottom: <?php echo get_field('t_padding_bottom').'px'; ?>;
            margin-top: <?php echo get_field('t_margin_top').'px'; ?>;
            margin-bottom: <?php echo get_field('t_margin_bottom').'px'; ?>;
        }
    <?php } ?>
    }
    /* lg (desktop) */
    @media (min-width: 992px) {
    <?php if (!get_field('show_on_desktop')){ ?>
        .text-boxes.<?php echo $block['id']; ?> {
            display: none;
        }
    <?php } else { ?>
        .text-boxes.<?php echo $block['id']; ?> {
            padding-top: <?php echo get_field('d_padding_top').'px'; ?>;
            padding-bottom: <?php echo get_field('d_padding_bottom').'px'; ?>;
            margin-top: <?php echo get_field('d_margin_top').'px'; ?>;
            margin-bottom: <?php echo get_field('d_margin_bottom').'px'; ?>;
        }
    <?php } ?>
    }

    /* custom styles */
    <?php echo get_field('custom_styles'); ?>

</style>
<section class="text-boxes <?php echo $block['id'] . ' ' . $block['className']; ?>" id="<?php echo $block['id']; ?>">
    <div class="<?php the_field('section_width'); ?>">
        <div class="row grid-row">
            <?php while(have_rows('text_boxes')){ the_row(); ?>
            <div class="col-sm-<?php the_field('m_grid'); ?> col-md-<?php the_field('t_grid'); ?> col-lg-<?php the_field('d_grid'); ?> text-box-wrapper">
                <div class="text-box txt-dark-blue">
                <?php the_sub_field('content'); ?>
                </div>
            </div>
            <?php }?>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?php $btn_count = 0; if (have_rows('buttons')) { ?>
                    <div class="buttons-wrapper">
                        <?php while (have_rows('buttons')) {
                            the_row(); ?>
                            <style>
                                .text-boxes.<?php echo $block['id']; ?> .buttons-wrapper{
                                    text-align: <?php the_sub_field('aligment'); ?>;
                                }
                            </style>
                            <a href="<?php echo get_sub_field('button')['url']; ?>" <?php echo get_sub_field('button')['target'] == '_blank' ? 'target="_blank"' : ''; ?>
                               class="btn btn-primary btn-<?php echo $btn_count; ?>"><?php echo get_sub_field('button')['title']; ?></a>
                            <?php $btn_count++; } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>