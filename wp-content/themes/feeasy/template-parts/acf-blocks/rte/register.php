<?php

$section_name = 'rte';
    acf_register_block_type(array(
    'name' => $section_name,
    'title' => __('RTE', 'wombat'),
    'description' => __('RTE section', 'wombat'),
    'category' => 'wombat', // common | formatting | layout | widgets | embed
    'icon' => 'layout',
    'keywords' => array('rte', 'text', 'mce', 'simple'),
    'post_types' => array('page', 'post'),
    'mode' => 'edit', // preview | edit | auto
    'align' => 'full', // left | center | right | wide | full
    'render_template' => get_template_directory() . '/template-parts/acf-blocks/'.$section_name.'/template.php',
    'enqueue_assets' => function(){
        wp_enqueue_style( 'rte', get_template_directory_uri() . '/template-parts/acf-blocks/rte/styles.css' );
        wp_enqueue_script( 'rte', get_template_directory_uri() . '/template-parts/acf-blocks/rte/scripts.js', array('jquery'), '', true );
    },
    'supports' => array(
        'align' => false, // array( 'left', 'right', 'full' ),
        'multiple' => true, // ture | false
        'mode' => false, // true | false
    ),
));

// Register Block Fields Group
include_once 'acf_field_group.php';