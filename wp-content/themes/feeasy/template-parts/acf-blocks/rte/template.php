<style media="all" type="text/css">

    /* general */
    <?php if (get_field('background_type')){ ?>
    .rte.<?php echo $block['id']; ?> {
    <?php if (get_field('background_type') == 'bg_image'){ ?> background-image: url("<?php echo wp_get_attachment_image_src(get_field('background_image'), 'full')[0]; ?>");
    <?php } else if (get_field('background_type') == 'bg_color'){?> background-color: <?php echo get_field('background_color'); ?>;
    <?php } ?>
    }

    <?php } ?>

    .rte.<?php echo $block['id']; ?>,
    .rte.<?php echo $block['id']; ?> p{
        color: <?php the_field('text_color'); ?>;
    }

    /* xs & sm (mobile) */
    @media (max-width: 767.98px) {
    <?php if (!get_field('show_on_mobile')){ ?>
        .rte.<?php echo $block['id']; ?> {
            display: none;
        }

    <?php } else { ?>
        .rte.<?php echo $block['id']; ?> {
            padding-top: <?php echo get_field('m_padding_top').'px'; ?>;
            padding-bottom: <?php echo get_field('m_padding_bottom').'px'; ?>;
            margin-top: <?php echo get_field('m_margin_top').'px'; ?>;
            margin-bottom: <?php echo get_field('m_margin_bottom').'px'; ?>;
        }

    <?php } ?>
    }

    /* md (tablet) */
    @media (min-width: 768px) and (max-width: 991.98px) {
    <?php if (!get_field('show_on_tablet')){ ?>
        .rte.<?php echo $block['id']; ?> {
            display: none;
        }

    <?php } else { ?>
        .rte.<?php echo $block['id']; ?> {
            padding-top: <?php echo get_field('t_padding_top').'px'; ?>;
            padding-bottom: <?php echo get_field('t_padding_bottom').'px'; ?>;
            margin-top: <?php echo get_field('t_margin_top').'px'; ?>;
            margin-bottom: <?php echo get_field('t_margin_bottom').'px'; ?>;
        }

    <?php } ?>
    }

    /* lg (desktop) */
    @media (min-width: 992px) {
    <?php if (!get_field('show_on_desktop')){ ?>
        .rte.<?php echo $block['id']; ?> {
            display: none;
        }

    <?php } else { ?>
        .rte.<?php echo $block['id']; ?> {
            padding-top: <?php echo get_field('d_padding_top').'px'; ?>;
            padding-bottom: <?php echo get_field('d_padding_bottom').'px'; ?>;
            margin-top: <?php echo get_field('d_margin_top').'px'; ?>;
            margin-bottom: <?php echo get_field('d_margin_bottom').'px'; ?>;
        }

    <?php } ?>
    }

    /* custom styles */
    <?php echo get_field('custom_styles'); ?>

</style>
<section class="rte <?php echo $block['id'] . ' ' . $block['className']; ?>"  id="<?php echo $block['id']; ?>">
    <div class="<?php the_field('section_width'); ?>">
        <div class="row">
            <div class="col-12">
                <?php the_field('content'); ?>

                <?php $btn_count = 0; if (have_rows('buttons')) { ?>
                    <div class="buttons-wrapper">
                        <?php while (have_rows('buttons')) {
                            the_row(); ?>
                            <style>
                                .rte.<?php echo $block['id']; ?> .buttons-wrapper{
                                    text-align: <?php the_sub_field('aligment'); ?>;
                                }
                            </style>
                            <a href="<?php echo get_sub_field('button')['url']; ?>" <?php echo get_sub_field('button')['target'] == '_blank' ? 'target="_blank"' : ''; ?>
                               class="btn btn-primary btn-<?php echo $btn_count; ?>"><?php echo get_sub_field('button')['title']; ?></a>
                        <?php $btn_count++; } ?>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
</section>