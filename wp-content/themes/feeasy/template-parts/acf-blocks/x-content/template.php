<style media="all" type="text/css">

    /* general */
    <?php if (get_field('background_type')){ ?>
    .x-content.<?php echo $block['id']; ?> {
    <?php if (get_field('background_type') == 'bg_image'){ ?> background-image: url("<?php echo wp_get_attachment_image_src(get_field('background_image'), 'full')[0]; ?>");
    <?php } else if (get_field('background_type') == 'bg_color'){?> background-color: <?php echo get_field('background_color'); ?>;
    <?php } ?>
    }

    <?php } ?>

    /* xs & sm (mobile) */
    @media (max-width: 767.98px) {
    <?php if (!get_field('show_on_mobile')){ ?>
        .x-content.<?php echo $block['id']; ?> {
            display: none;
        }

    <?php } else { ?>
        .x-content.<?php echo $block['id']; ?> {
            padding-top: <?php echo get_field('m_padding_top').'px'; ?>;
            padding-bottom: <?php echo get_field('m_padding_bottom').'px'; ?>;
            margin-top: <?php echo get_field('m_margin_top').'px'; ?>;
            margin-bottom: <?php echo get_field('m_margin_bottom').'px'; ?>;
        }

    <?php } ?>
    }

    /* md (tablet) */
    @media (min-width: 768px) and (max-width: 991.98px) {
    <?php if (!get_field('show_on_tablet')){ ?>
        .x-content.<?php echo $block['id']; ?> {
            display: none;
        }

    <?php } else { ?>
        .x-content.<?php echo $block['id']; ?> {
            padding-top: <?php echo get_field('t_padding_top').'px'; ?>;
            padding-bottom: <?php echo get_field('t_padding_bottom').'px'; ?>;
            margin-top: <?php echo get_field('t_margin_top').'px'; ?>;
            margin-bottom: <?php echo get_field('t_margin_bottom').'px'; ?>;
        }

    <?php } ?>
    }

    /* lg (desktop) */
    @media (min-width: 992px) {
    <?php if (!get_field('show_on_desktop')){ ?>
        .x-content.<?php echo $block['id']; ?> {
            display: none;
        }

    <?php } else { ?>
        .x-content.<?php echo $block['id']; ?> {
            padding-top: <?php echo get_field('d_padding_top').'px'; ?>;
            padding-bottom: <?php echo get_field('d_padding_bottom').'px'; ?>;
            margin-top: <?php echo get_field('d_margin_top').'px'; ?>;
            margin-bottom: <?php echo get_field('d_margin_bottom').'px'; ?>;
        }

    <?php } ?>
    }

    /* custom styles */
    <?php echo get_field('custom_styles'); ?>

</style>
<section class="x-content <?php echo $block['id'] . ' ' . $block['className']; ?>" id="<?php echo $block['id']; ?>">
    <div class="container">
        <?php $row_count = 0;
        while (have_rows('content-repeater')) {
        the_row(); ?>
        <?php if (get_sub_field('heading')) {
            $htag1 = '<' . get_sub_field('h_tag') . ' class="txt-sm txt-gray section-head">';
            $htag2 = '</' . get_sub_field('h_tag') . '>';
        } ?>
        <?php
        $orientation = get_sub_field('orientation');
        ?>
        <div class="row row-<?php echo $row_count; ?>" id="<?php echo $block['id'].'-'.$row_count; ?>">
            <?php if (get_sub_field('heading')) { ?>
                <div class="col-sm-12"><?php echo $htag1 . get_sub_field('heading') . $htag2; ?></div>
            <?php } ?>
        </div>
        <div class="row row-<?php echo $row_count; ?> <?php echo $orientation; ?>">
            <div class="col-xs-12 col-sm-12 col-md-5 textarea-wrapper">
                <?php the_sub_field('text_area'); ?>
                <?php $btn_count = 0;
                if (get_sub_field('button')) { ?>
                    <div class="buttons-wrapper">
                        <a href="<?php echo get_sub_field('button')['url']; ?>" <?php echo get_sub_field('button')['target'] == '_blank' ? 'target="_blank"' : ''; ?>
                           class="btn btn-primary btn-<?php echo $btn_count; ?>"><?php echo get_sub_field('button')['title']; ?></a>
                    </div>
                <?php } ?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 image-wrapper">
                <?php echo wp_get_attachment_image(get_sub_field('image'), 'full', false, array('alt'=>get_sub_field('heading'))); ?>
            </div>
        </div>
    <?php $row_count++;
    } ?>
    </div>
</section>