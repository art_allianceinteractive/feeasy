<?php

$section_name = 'x-content';
    acf_register_block_type(array(
    'name' => $section_name,
    'title' => __('X-content', 'wombat'),
    'description' => __('X-content', 'wombat'),
    'category' => 'wombat', // common | formatting | layout | widgets | embed
    'icon' => 'layout',
    'keywords' => array('x-content', 'text', 'mce', 'simple'),
    'post_types' => array('page', 'post'),
    'mode' => 'edit', // preview | edit | auto
    'align' => 'full', // left | center | right | wide | full
    'render_template' => get_template_directory() . '/template-parts/acf-blocks/'.$section_name.'/template.php',
    'enqueue_assets' => function(){
        wp_enqueue_style( 'x-content', get_template_directory_uri() . '/template-parts/acf-blocks/x-content/styles.css' );
        wp_enqueue_script( 'x-content', get_template_directory_uri() . '/template-parts/acf-blocks/x-content/scripts.js', array('jquery'), '', true );
    },
    'supports' => array(
        'align' => false, // array( 'left', 'right', 'full' ),
        'multiple' => true, // ture | false
        'mode' => false, // true | false
    ),
));

// Register Block Fields Group
include_once 'acf_field_group.php';