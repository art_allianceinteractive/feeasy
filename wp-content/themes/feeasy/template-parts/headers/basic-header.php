<div class="container-fluid basic-header <?php echo get_field('custom_logo') ? 'custom-logo' : ''?>">
    <div class="row">
        <div class="col-md-2 logo-wrapper">
            <a href="<?php echo home_url(); ?>">
                <?php
                if(!get_field('custom_logo')){
                $logo_img = wp_get_attachment_image_url(get_field('logo_dark', 'options'), array(215,110));
                ?>
                <img src="<?php echo $logo_img; ?>" alt="<?php echo bloginfo('name'); ?>">
                <?php } else { ?>
                    <div class="top-logo">
                        <?php $logo_top_img = wp_get_attachment_image_url(get_field('custom_logo_top'), 'custom-logo'); ?>
                        <img src="<?php echo $logo_top_img; ?>" alt="<?php echo bloginfo('name'); ?>">
                    </div>
                    <div class="bottom-logo">
                        <?php $logo_bottom_img = wp_get_attachment_image_url(get_field('custom_logo_bottom'), 'custom-sub-logo'); ?>
                        <span><?php the_field('custom_logo_caption'); ?></span>
                        <img src="<?php echo $logo_bottom_img; ?>" alt="<?php echo bloginfo('name'); ?>">
                    </div>
                <?php } ?>
            </a>
        </div>
        <div class="col-md-10 nav-wrapper">
                <?php
                wp_nav_menu( array(
                    'theme_location'    => "top-menu",
                    'menu_class'   => "txt-white txt-xs txt-upper",
                    'item_spacing'      => "discard"
                ) );
                wp_nav_menu( array(
                    'theme_location'    => "header-menu",
                    'menu_class'   => "txt-white txt-md-lg",
                    'item_spacing'      => "discard"
                ) );
                ?>
        </div>
        <a href="#" class="nav-open">
            <div></div>
            <div></div>
            <div></div>
        </a>
        <div class="col-md-10 nav-wrapper-mobile">
            <a href="#" class="nav-close"></a>
            <?php
            wp_nav_menu( array(
                'theme_location'    => "header-menu",
                'menu_class'   => "txt-white txt-md-lg",
                'item_spacing'      => "discard"
            ) );
            wp_nav_menu( array(
                'theme_location'    => "top-menu",
                'menu_class'   => "txt-white txt-xs txt-upper",
                'item_spacing'      => "discard"
            ) );
            ?>
        </div>
    </div>
</div>