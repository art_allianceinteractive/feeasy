<?php
while(have_rows('full_screen_header')){
    the_row();

    $logo_img = '';

    if(get_sub_field('color_theme')[0] == 'light'){
        $humb_class="light-humb";
    } else if (get_sub_field('color_theme')[0] == 'dark') {
        $humb_class="dark-humb";
    }
    $logo_img = wp_get_attachment_image_url(get_field('logo_light', 'options'), array(215,110));
    $logo_sticky_img = wp_get_attachment_image_url(get_field('logo_dark', 'options'), array(215,110));
    $hero_img = wp_get_attachment_image_url(get_sub_field('image'), 'full');
    $scroll_to = get_sub_field('scroll_to');
}
?>

<style>
    .container-fluid.full-screen-header.full-height{
        background: url("<?php echo $hero_img; ?>");
        background-size: cover;
        background-position: center center;
    }

    /* xs & sm (mobile) */
    @media (max-width: 767.98px) {
        .container-fluid.full-screen-header.full-height{
            height: 400px;
            background-position: center center;
        }
    }

</style>
<div class="container-fluid full-screen-header full-height">
    <div class="row ">
        <div class="col-md-2 logo-wrapper">
            <a href="<?php echo home_url(); ?>">
                <img class="top-logo" src="<?php echo $logo_img; ?>" alt="<?php echo bloginfo('name'); ?>">
                <img class="sticky-logo" src="<?php echo $logo_sticky_img; ?>" alt="<?php echo bloginfo('name'); ?>">
            </a>
        </div>
        <div class="col-md-10 nav-wrapper">
                <?php
                wp_nav_menu( array(
                    'theme_location'    => "top-menu",
                    'menu_class'   => "txt-white txt-xs txt-upper",
                    'item_spacing'      => "discard"
                ) );
                wp_nav_menu( array(
                    'theme_location'    => "header-menu",
                    'menu_class'   => "txt-white txt-md-lg",
                    'item_spacing'      => "discard"
                ) );
                ?>
        </div>
        <a href="#" class="nav-open <?php echo $humb_class; ?>">
            <div></div>
            <div></div>
            <div></div>
        </a>
        <div class="col-md-10 nav-wrapper-mobile">
            <a href="#" class="nav-close"></a>
            <?php
            wp_nav_menu( array(
                'theme_location'    => "header-menu",
                'menu_class'   => "txt-white txt-md-lg",
                'item_spacing'      => "discard"
            ) );
            wp_nav_menu( array(
                'theme_location'    => "top-menu",
                'menu_class'   => "txt-white txt-xs txt-upper",
                'item_spacing'      => "discard"
            ) );
            ?>
        </div>

    </div>
</div>
<a href="#<?php echo $scroll_to; ?>" class="roll-button"></a>