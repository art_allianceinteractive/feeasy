<?php /* Template Name: Vendor Area Page */
get_header();
vendors_only('redirect');
?>

    <section class="container-fluid vendor-area">
        <div class="row">
            <div class="col-lg-3 vendor-nav-wrapper">
                <ul class="vendor-nav">
                    <li><a href="<?php echo get_field('vendor_office_page', 'options'); ?>" class="active">forms</a>
                    </li>
                    <li><a href="<?php echo get_field('vendor_profile_page', 'options'); ?>">profile</a></li>
                    <li><a href="<?php echo wp_logout_url(get_home_url()); ?>">log out</a></li>
                </ul>
            </div>
        </div>
    </section>
    <div class="container-fluid vendor-forms-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <table id="vendor-forms" class="display" style="width:100%">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>State</th>
                        <th>Loan Purpose</th>
                        <th>Loan Amount</th>
                        <th>Heard Of Us</th>
                        <th>Referred By</th>
                        <th>Result</th>
                        <th>Offer Page Link</th>
                        <th>Date Submitted</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php


                    $vendor_user_id = get_current_user_id();
                    $vendor_data_id = '';

                    $vendors = get_posts(array(
                        'numberposts' => -1,
                        'post_type' => 'vendors',
                        'meta_query' => array(
                            array(
                                'key' => 'vendor_login_user',
                                'value' => $vendor_user_id,
                                'compare' => '=',
                            )
                        )
                    ));

                    foreach ($vendors as $vendor) {
                        $vendor_data_id = $vendor->ID;
                    }

                    $borrowers = get_posts(array(
                        'numberposts' => -1,
                        'post_type' => 'borrowers',
                        'meta_query' => array(
                            array(
                                'key' => 'borrower_referred_by',
                                'value' => $vendor_data_id,
                                'compare' => '=',
                            )
                        )
                    ));

                    foreach ($borrowers as $borrower) {
                        ?>
                        <tr>
                            <td><?php the_field('borrower_first_name', $borrower->ID); ?> <?php the_field('borrower_last_name', $borrower->ID); ?></td>
                            <td><?php the_field('borrower_email', $borrower->ID); ?></td>
                            <td><?php the_field('borrower_primary_phone', $borrower->ID); ?></td>
                            <td><?php the_field('borrower_state', $borrower->ID); ?></td>
                            <td><?php the_field('borrower_loan_purpose', $borrower->ID); ?></td>
                            <td><?php the_field('borrower_loan_amount', $borrower->ID); ?></td>
                            <td><?php the_field('borrower_way_you_find_us', $borrower->ID); ?></td>
                            <td><?php echo get_the_title(get_field('borrower_referred_by', $borrower->ID)); ?></td>
                            <td><?php the_field('borrower_api_status', $borrower->ID); ?></td>
                            <td>
                                <?php if (get_field('borrower_api_redirect_url', $borrower->ID)) { ?>
                                    <a href="<?php the_field('borrower_api_redirect_url', $borrower->ID); ?>"
                                       target="_blank">Offer
                                        Page Link</a>
                                <?php } ?></td>
                            <td><?php echo get_the_date('Y-m-d H:i:s', $borrower->ID); ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>State</th>
                        <th>Loan Purpose</th>
                        <th>Loan Amount</th>
                        <th>Heard Of Us</th>
                        <th>Referred By</th>
                        <th>Result</th>
                        <th>Redirect Url</th>
                        <th>Created Date</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
<?php
get_footer();
?>