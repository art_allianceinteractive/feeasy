<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta itemprop="name" content="<?php bloginfo('name'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <?php wp_head(); ?>
    <?php vendor_autocomplete(); ?>
</head>
<body <?php body_class(); ?> >
<main role="main">

    <?php if (get_post_type() == 'vendors' || get_post_type() == 'borrowers'){
        wp_redirect(home_url());
    }?>

    <?php
    if (get_field('header_type') == 'full_screen') {
        get_template_part('template-parts/headers/full-screen-header');
    } else if (get_field('header_type') == 'basic') {
        get_template_part('template-parts/headers/basic-header');
    } ?>

