<?php
//cF1bbwMGCwdHTnc live
/*-------------------------------------------------*/
/*	Updates
/*-------------------------------------------------*/
add_filter('allow_dev_auto_core_updates', '__return_false');
add_filter('allow_minor_auto_core_updates', '__return_false');
add_filter('allow_major_auto_core_updates', '__return_false');
add_filter('auto_update_plugin', '__return_false');
add_filter('auto_core_update_send_email', '__return_false');
add_filter('auto_update_theme', '__return_false');

/*-------------------------------------------------*/
/*	Theme Clean Up
/*-------------------------------------------------*/
function feeasy_head_cleanup()
{
    // flush_rewrite_rules( );
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'index_rel_link');
    remove_action('wp_head', 'feed_links', 2);
    remove_action('wp_head', 'parent_post_rel_link', 10, 0);
    remove_action('wp_head', 'start_post_rel_link', 10, 0);
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');
}

add_action('init', 'feeasy_head_cleanup');

/*-------------------------------------------------*/
/*	Theme Setup
/*-------------------------------------------------*/

if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Theme Header Settings',
        'menu_title' => 'Header',
        'parent_slug' => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Theme Footer Settings',
        'menu_title' => 'Footer',
        'parent_slug' => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Theme Social Networks Settings',
        'menu_title' => 'Social',
        'parent_slug' => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Monevo API Settings',
        'menu_title' => 'Monevo API',
        'parent_slug' => 'theme-general-settings',
    ));
    acf_add_options_sub_page(array(
        'page_title' => 'Email Notifications',
        'menu_title' => 'Email Notifications',
        'parent_slug' => 'theme-general-settings',
    ));

}

function feeasy_setup()
{
    add_theme_support('html5');
    add_theme_support('title-tag');
    add_theme_support('menus');
    add_theme_support('post-thumbnails', array('post', 'bio'));
    /*
    add_theme_support('custom-logo', array(
        'height' => 67,
        'width' => 300,
        'flex-height' => true,
        'flex-width' => true,
        'header-text' => array('site-title', 'site-description'),
    ));
    */
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'custom-logo', 560, 103, false ); //resize, crop in functions.php
    add_image_size( 'custom-sub-logo', 230, 48, false ); //resize, crop in functions.php
    register_nav_menu('top-menu', __('Top Menu'));
    register_nav_menu('header-menu', __('Header Menu'));
    register_nav_menu('footer-menu-top', __('Footer Menu Top'));
    register_nav_menu('footer-menu-bottom', __('Footer Menu Bottom'));

    show_admin_bar(false);
}

add_action('after_setup_theme', 'feeasy_setup');

function gf_entry_normalize($entry, $form)
{
    $normalized = array();
    foreach ($form['fields'] as $field_data) {
        if (is_array($field_data->inputs)){
            foreach ($field_data->inputs as $subfield_data) {
                $normalized[sanitize_title($field_data->label)][sanitize_title($subfield_data['label'])] = $entry[$subfield_data['id']];
            }
        } else {
        $normalized[sanitize_title($field_data->label)] = $entry[$field_data->id];
        }
    }
    return $normalized;
}

function getUserIP()
{
    // Get real visitor IP behind CloudFlare network
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
        $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
}

// Disable auto-complete on form.
add_filter( 'gform_form_tag', function( $form_tag ) {
    return str_replace( '>', ' autocomplete="off">', $form_tag );
}, 11 );

// Diable auto-complete on each field.
add_filter( 'gform_field_content', function( $input ) {
    return preg_replace( '/<(input|textarea)/', '<${1} autocomplete="off" ', $input );
}, 11 );

/*-------------------------------------------------*/
/*	Scripts && Styles
/*-------------------------------------------------*/

function feeasy_enqueues()
{
    $rand = rand(00000000, 99999999);

    //Styles
    wp_enqueue_style('theme_reset_styles', get_template_directory_uri() . '/assets/css/reset.css', array(), $rand, 'all');
    wp_enqueue_style('theme_fonts', get_template_directory_uri() . '/assets/css/fonts.css', array(), $rand, 'all');
    wp_enqueue_style('theme_grid', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), $rand, 'all');
    wp_enqueue_style('theme_styles', get_template_directory_uri() . '/assets/css/style.css', array(), $rand, 'all');
    wp_enqueue_style('theme_basic_header_styles', get_template_directory_uri() . '/assets/css/basic-header.css', array(), $rand, 'all');
    wp_enqueue_style('theme_single_styles', get_template_directory_uri() . '/assets/css/single.css', array(), $rand, 'all');
    wp_enqueue_style('vendor_login_styles', get_template_directory_uri() . '/assets/css/vendor-login.css', array(), $rand, 'all');
    wp_enqueue_style('vendor_area_styles', get_template_directory_uri() . '/assets/css/vendor-area.css', array(), $rand, 'all');

    wp_enqueue_style('tables_styles', 'https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css', array(), $rand, 'all');

    //Remove basic blocks styles
    wp_dequeue_style('wp-block-library');
    wp_dequeue_style('wp-block-library-theme');
    wp_dequeue_style('wc-block-style'); // Remove WooCommerce block CSS

    //Scripts
    wp_enqueue_script('scripts-js', get_template_directory_uri() . '/assets/js/scripts.js', array(), false, true);

    wp_enqueue_script('tables-js', 'https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js', array(), false, true);

    wp_localize_script( 'scripts-js', 'MyAjax', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'security' => wp_create_nonce( 'ml456knjdjf34n3fn309m93' )
    ));

}

function feeasy_admin_enqueues()
{
    $rand = rand(00000000, 99999999);
    wp_enqueue_script('tables-js', 'https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js', array(), false, true);
    wp_enqueue_style('tables_styles', 'https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css', array(), $rand, 'all');
    wp_enqueue_style('theme_admin_styles', get_template_directory_uri() . '/admin/assets/css/admin-styles.css', array(), $rand, 'all');
    wp_enqueue_script('theme_admin_scripts', get_template_directory_uri() . '/admin/assets/js/admin-scripts.js', array(), false, true);
}

add_action('wp_enqueue_scripts', 'feeasy_enqueues');
add_action('admin_enqueue_scripts', 'feeasy_admin_enqueues');

/*-------------------------------------------------*/
/*	ACF
/*-------------------------------------------------*/

add_filter('the_content', function ($content) {
    if (has_blocks()) {
        return $content;
    }
    return wpautop($content);
});

/*-------------------------------------------------*/
/*	ACF blocks
/*-------------------------------------------------*/
require get_template_directory() . '/inc/register-acf-blocks.php';


/*-------------------------------------------------*/
/*	Need to order
/*-------------------------------------------------*/
function add_additional_class_on_li($classes, $item, $args)
{
    if (isset($args->add_li_class)) {
        $classes = array('nav-item');
    }
    return $classes;
}

add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);

function add_specific_menu_location_atts($atts, $item, $args)
{
    $atts['class'] = 'nav-link';
    return $atts;
}

add_filter('nav_menu_link_attributes', 'add_specific_menu_location_atts', 10, 3);


/*-------------------------------------------------*/
/*	vendor only access
/*-------------------------------------------------*/

function vendors_only($action)
{
    $user_check = wp_get_current_user();

    if (!isset($user_check->roles) || !is_array($user_check->roles) || !in_array('vendor', $user_check->roles)) {
        if ($action == 'redirect') {
            wp_redirect(home_url());
            exit;
        } else {
            return false;
        }
    } else {
        if ($action != 'redirect') {
            return true;
        }
    }
}


function admin_access()
{
    if (!defined('DOING_AJAX') || !DOING_AJAX) {
        $user = wp_get_current_user();

        if (isset($user->roles) && is_array($user->roles)) {
            if (in_array('editor', $user->roles)) {
                echo "";
            } else if (in_array('administrator', $user->roles)) {
                echo "";
            } else if (in_array('admin_lite', $user->roles)) {
                echo "";
            } else if (in_array('wpseo_editor', $user->roles)) {
                echo "";
            } else if (in_array('wpseo_manager', $user->roles)) {
                echo "";
            } else if (in_array('vendor', $user->roles)) {
                wp_redirect(get_field('vendor_office_page','options'));
            } else {
                wp_redirect(home_url());
                die;
            }
        }
    }
}

add_action('admin_init', 'admin_access');

/*-------------------------------------------------*/
/*	account nav
/*-------------------------------------------------*/
add_filter('wp_nav_menu_items', 'add_user_link', 10, 2);
function add_user_link($items, $args)
{
    if ($args->theme_location == 'top-menu') {
        if (is_user_logged_in() && vendors_only('check')) {
            $items = str_replace('<a href="http://auth" class="nav-link">[auth]</a>', '<a href="' . get_field('vendor_office_page', 'options') . '" class="user-email">' . wp_get_current_user()->user_email . '</a> <a href="' . wp_logout_url(get_home_url()) . '" class="nav-link">(Log out)</a>', $items );
            //$items .= '<li id="menu-item-58" class="login-icon menu-item menu-item-type-post_type menu-item-object-page menu-item-58"><a href="' . get_field('vendor_office_page', 'options') . '" class="user-email">' . wp_get_current_user()->user_email . '</a> <a href="' . wp_logout_url(get_home_url()) . '" class="nav-link">(Log out)</a></li>';
        } else {
            $items = str_replace('<a href="http://auth" class="nav-link">[auth]</a>', '<a href="' . get_field('vendor_login_page', 'options') . '" class="nav-link">Vendor Login</a>', $items);
            //$items .= '<li id="menu-item-58" class="login-icon menu-item menu-item-type-post_type menu-item-object-page menu-item-58"><a href="' . get_field('vendor_login_page', 'options') . '" class="nav-link">Vendor Login</a></li>';
        }

        $items .= '<li id="menu-item-59" class="menu-item menu-item-type-post_type menu-item-object-page mobile-only menu-item-59"><a href="' . get_field('advertiser_disclosure_page', 'options') . '" class="nav-link">Advertiser Disclosure</a></li>';

    }
    return $items;
}

/*-------------------------------------------------*/
/*	Vendor Backend
/*-------------------------------------------------*/
include "admin/inc/vendors.php";

/*-------------------------------------------------*/
/*	Borrower Backend
/*-------------------------------------------------*/
include "admin/inc/borrowers.php";

/*-------------------------------------------------*/
/*	Lead Backend
/*-------------------------------------------------*/
include "admin/inc/leads.php";

/*-------------------------------------------------*/
/*	login, reset, lost
/*-------------------------------------------------*/
include "inc/login.php";

/*-------------------------------------------------*/
/*	Monevo API
/*-------------------------------------------------*/
include "inc/monevo_api.php";

/*-------------------------------------------------*/
/*	Vendor Autocomplete
/*-------------------------------------------------*/
include "inc/vendor-autocomplete.php";


//loan_request(array());