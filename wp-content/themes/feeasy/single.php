<?php get_header(); ?>
<article class="container">
    <h1><?php the_title(); ?></h1>
    <?php echo get_the_post_thumbnail(); ?>
<?php the_content(); ?>
</article>
<?php get_footer(); ?>